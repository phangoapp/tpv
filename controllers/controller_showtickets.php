<?php

use PhangoApp\Tpv\LoginController;
use PhangoApp\PhaView\View;
use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaTime\DateTimeNow;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\GenerateAdminClass;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

class ShowTicketsController extends LoginController {

    public function home()
    {
        settype($_GET['op'], 'integer');
        
        switch($_GET['op'])
        {
            
            default:
    
                $user=new UserEnterprise();
                    
                $bill=new Bill();
                
                $arr_user=$user->select_a_row($_SESSION['id']);
                
                $admin=new GenerateAdminClass($bill, '');
                
                $admin->no_insert=true;
                
                $admin->no_delete=true;
                
                $admin->list->arr_fields_search=['id'];
                
                $admin->list->order_field='date';
                
                $admin->list->order=1;
            
                $admin->list->arr_fields_showed=['id', 'total_price', 'payment', 'date', 'table_id', 'cancelled'];
                
                $admin->list->yes_search=true;
                
                $admin->list->options_func='bill_options';
            
                echo View::load_view([$arr_user, $admin], 'tpv/showtickets');
                
            break;
            
            case 1:
            
                settype($_GET['bill_id'], 'integer');
                
                $bill=new Bill();
                
                $bill->create_forms();
                
                $bill->reset_require();
                
                $bill->where(['where id=?', [$_GET['bill_id']]])->update(['payment' => 0]);
                
                $log=new LogTpv();
        
                $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'reopen_bill', 'Reopen bill nº').' '.$_GET['bill_id']);
                
                header('Location: '.Routes::get_url('tpv', [], ['bill_id' => $_GET['bill_id']]));
                
                die;
            
            break;
            
            case 2:
            
                settype($_GET['bill_id'], 'integer');
                
                $bill=new Bill();
                
                $bill->create_forms();
                
                $bill->reset_require();
                
                $bill->where(['where id=?', [$_GET['bill_id']]])->update(['cancelled' => 1, 'payment' => 0]);
                
                $log=new LogTpv();
        
                $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'cancelled_bill', 'Cancelled bill nº').' '.$_GET['bill_id']);
                
                header('Location: '.Routes::get_url('tpv/showtickets'));
            
            break;
            
            case 3:
            
                settype($_GET['bill_id'], 'integer');
                
                $bill=new Bill();
                
                $bill->create_forms();
                
                $bill->reset_require();
                
                $bill->where(['where id=?', [$_GET['bill_id']]])->update(['cancelled' => 0, 'payment' => 0, 'money_paid' => 0]);
                
                $log=new LogTpv();
        
                $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'reopened_bill', 'Reopened bill nº').' '.$_GET['bill_id']);
                
                header('Location: '.Routes::get_url('tpv/showtickets'));
            
            break;
        }
    }

}

function bill_options($url_options, $model_name, $id, $arr_row)
{
 
    $arr_options=[];
    
    if(!$arr_row['cancelled'])
    {
    
        $arr_options[]='<a href="'.Routes::get_url('tpv/showtickets', [], ['op' => 1, 'bill_id' => $id]).'">'.I18n::lang('common', 'edit', 'Edit').'</a>';
        
        $arr_options[]='<a class="cancel_bill" id="cancel_bill_'.$id.'"  href="'.Routes::get_url('tpv/showtickets', [], ['op' => 2, 'bill_id' => $id]).'">'.I18n::lang('common', 'cancel', 'Cancel').'</a>';

    }
    else
    {
        $arr_options[]='<a class="reopen_bill" href="'.Routes::get_url('tpv/showtickets', [], ['op' => 3, 'bill_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'reopen', 'Reopen').'</a>';
        
    }
    
    return $arr_options;
    
}
