<?php

use PhangoApp\Tpv\LoginController;
use PhangoApp\PhaView\View;
use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaTime\DateTimeNow;
use PhangoApp\PhaRouter\Routes;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

class IndexController extends LoginController {

    public function home() 
    {
        $openbox=new OpenBox();
        
        //Get now date. 
        
        //Check if box is open, if open and is other day, say that
        //If no open, open in a controller
        
       $arr_box=$openbox->where(['WHERE closed=? and enterprise_id=?', [0, $_SESSION['enterprise_id']]])->select_a_row_where();
       
       if($arr_box)
       {
            $close_box=0;
           
            $now=DateTimeNow::$today_first;
            
            if($arr_box['date']!=$now)
            {
                
                $close_box=1;
                
            }
        
            $categories=new CategoryProduct();
            
            $table=new TableBar();
        
            $user=new UserEnterprise();
            
            $bill=new Bill();
            
            $date_check=substr($arr_box['date'], 0, 8);
            
            $q=$bill->execute('select SUM(total_price) from bill WHERE payment=1 AND openbox_id=?', [$arr_box['id']]);
            
            $total_price_raw=$bill->fetch_row($q)[0];
            
            $total_price=$bill->components['total_price']->currency_format($total_price_raw);

            $arr_table=$table->where(['WHERE `default`=? AND enterprise_id=?', [1, $_SESSION['enterprise_id']]])->select_a_row_where();
        
            settype($arr_table['id'], 'integer');
            
            $arr_user=$user->select_a_row($_SESSION['id']);
            
            $form_categories=new PhangoApp\PhaModels\Forms\SelectModelFormByOrder('category_id', '', $categories, 'name', 'parent', $where=['WHERE 1=1', []], $null_yes=1);
        
            $form_categories->extra_param=' size="10" style="display:none;"';
            
            $form_tables=new PhangoApp\PhaModels\Forms\SelectModelForm('table_id', $arr_table['id'], new TableBar(), 'name', 'id');
        
            $form_tables->extra_param='';
            
            $form_tables->conditions=['WHERE enterprise_id=?', [$_SESSION['enterprise_id']]];
        
            echo View::load_view([$arr_user, $form_categories, $form_tables, $close_box, $total_price, $total_price_raw], 'tpv/tpv');
            
        }
        else
        {
            
            echo View::load_view([], 'tpv/openbox');
            
        }
        
    }
    
    public function get_total_billed()
    {
        
        //$now=DateTimeNow::$today_first;
        
        $openbox=new OpenBox();
        
        //Get now date. 
        
        //Check if box is open, if open and is other day, say that
        //If no open, open in a controller
        
        $arr_box=$openbox->where(['WHERE closed=? and enterprise_id=?', [0, $_SESSION['enterprise_id']]])->select_a_row_where();
        
        $now=$arr_box['date'];
        
        $date_check=substr($now, 0, 8);
        
        $bill=new Bill();
        
        $arr_data['error']=0;
        
        $q=$bill->execute('select SUM(total_price) from bill WHERE payment=1 AND openbox_id=', [$arr_box['id']]);
            
        $arr_data['total_billed_raw']=$bill->fetch_row($q)[0];
        
        settype($arr_data['total_billed_raw'], 'integer');
        
        header('Content-type: application/json');
        
        echo json_encode($arr_data);
        
        die;
        
    }
    
    public function open_box()
    {
        
        $now=DateTimeNow::$today_first;
        
        $arr_data=['error' => 1, 'txt_error' => 'Error:cannot open the box'];
        
        //Open box
        
        $openbox=new OpenBox();
        
        //Get now date. 
        
        //Check if box is open, if open and is other day, say that
        //If no open, open in a controller
        
        $c=$openbox->where(['WHERE closed=? and enterprise_id=?', [0, $_SESSION['enterprise_id']]])->select_count();
       
        if($c==0)
        {
        
            $now=DateTimeNow::$today_first;
            
            $openbox->create_forms();
            
            $openbox->components['date']->to_utc=false;
        
            //If exists a box closed with the same date, reopen.
            
            $insert='insert';
            $id=0;
            
            $arr_closed_today=$openbox->where(['WHERE date=?', [$now]])->select_a_row_where();
            
            $message=I18n::lang('phangoapp/tpv', 'open_box', 'open box');
            
            if($arr_closed_today)
            {

                $insert='update';
                
                $id=$arr_closed_today['id'];
                
                $message=I18n::lang('phangoapp/tpv', 'reopen_box', 'reopen box');
                
            }

            if($openbox->where(['WHERE id=?', [$id]])->$insert(['date' => $now, 'closed' => 0, 'enterprise_id' => $_SESSION['enterprise_id']]))
            {
                
                $log=new LogTpv();
                
                $log->log($_SESSION['name'].': '.$message);
                
                $arr_data['error']=0;
                $arr_data['txt_error']='';
                
            }
        
        }
        
        header('Content-type: application/json');
        echo json_encode($arr_data);
        die;
    }
    
    public function close_box()
    {
        
        $openbox=new OpenBox();
        
        $openbox->create_forms();
        
        $openbox->where(['WHERE closed=? and enterprise_id=?', [0, $_SESSION['enterprise_id']]])->update(['closed' => 1]);
        
        $log=new LogTpv();
        
        $message=I18n::lang('phangoapp/tpv', 'close_box', 'close box');
        
        $log->log($_SESSION['name'].': '.$message);        
        
        header('Location: '.Routes::get_url('tpv'));
        
        die;
        
    }
    
    public function get_products()
    {
        
        
        settype($_GET['category_id'], 'integer');
        
        $products=new Products();
        
        $arr_products=[];
        
        $order=['units_solded' => 1, 'name' => 0];
        
        if($_GET['category_id']==0)
        {
            
            $products->set_order($order);
            $products->set_limit([8]);
            $products->set_conditions(['WHERE (products.id IN (select product_id from stock where enterprise_id=? and avaliable=?))', [ $_SESSION['enterprise_id'], 1]]);
            
        }
        else
        {

            $products->set_order(['name' => 0]);
            $products->set_conditions(['WHERE (products.id IN (select product_id from stock where enterprise_id=? and avaliable=?))  AND (category_id=? OR category_id IN (select id from categoryproduct where parent=?))', [ $_SESSION['enterprise_id'], 1, $_GET['category_id'],$_GET['category_id']]]);
            
        }
        
        $query=$products->select();
        
        while($arr_product=$products->fetch_array($query))
        {
            
            $arr_product['image']=$products->components['image']->show_image_url('mini_'.$arr_product['image']);
            
            $arr_product['price']='€ '.number_format($arr_product['base_price']/100, 2, ',', '.');
            
            $arr_products[]=$arr_product;
            
        }
        
        header('Content-type: application/json');
        
        echo json_encode($arr_products);
        
        die;
        
    }

    public function save_payment()
    {
        
        $arr_error['error']=1;
        
        if(isset($_POST['products']))
        {
            //Insert bill
            
            $bill=new Bill();
            $bill->create_forms();
            $product_bill=new ProductBill();
            $product_bill->create_forms();
            $error=0;
            
            settype($_POST['money_paid'], 'integer');
            settype($_POST['table_id'], 'integer');
            settype($_POST['bill_id'], 'integer');
            
            $func_update='insert';
            
            if($_POST['bill_id']>0)
            {
                
                $func_update='update';
                
                //Regenerate products.
                
                $product_bill->where(['WHERE bill_id=?', [$_POST['bill_id']]])->delete();
                
                $bill->set_conditions(['WHERE id=?', [$_POST['bill_id']]]);
                
                $message=I18n::lang('phangoapp/tpv', 'updated_bill', 'Updated bill nº ');
                
                function get_id($bill)
                {
                    
                    return $_POST['bill_id'];
                    
                }
                
            }
            else
            {
                $message=I18n::lang('phangoapp/tpv', 'added_bill', 'Added new bill nº ');
                
                function get_id($bill)
                {
                    
                    return $bill->insert_id();
                    
                }
                
            }
            
            $log=new LogTpv();
            
            $openbox=new OpenBox();
            
            $arr_box=$openbox->where(['WHERE closed=?', [0]])->select_a_row_where();
            
            $month=substr($arr_box['date'], 0, 6);
            
            if($bill->$func_update(['money_paid' => $_POST['money_paid'], 'date' => 0, 'month' => $month, 'openbox_id' => $arr_box['id'], 'payment' => 0, 'table_id' => $_POST['table_id'], 'total_price' => $_POST['total_price'], 'enterprise_id' => $_SESSION['enterprise_id'], 'neto_price' => $_POST['neto_price'], 'tax_name' => $_POST['tax_name'], 'tax_price' => $_POST['tax_price'], 'discount_price' => $_POST['discount_price']]))
            {
                
                $new_bill_id=get_id($bill);
                    
                //Insert products
                $error_product='';
                
                foreach($_POST['products'] as $product)
                {
                    
                    $product['bill_id']=$new_bill_id;
                    $product['total_price_units']=$product['total_price']*$product['units'];
                    
                    if(!$product_bill->insert($product))
                    {
                        $error_product.=' '.$product_bill->std_error;
                        $error++;
                        break;
                        
                    }
                    
                }
                
                $arr_error['bill_id']=$new_bill_id;
                
                if($error>0)
                {
                    
                    $arr_error['txt_error']=I18n::lang('phangoapp/tpv', 'error_creating_bill', 'Error: cannot add product to the new bill ->'.$error_product);
                        
                    $bill->delete($new_bill_id);
        
                    $message=I18n::lang('phangoapp/tpv', 'error_adding_product', 'Error adding product');
                    
                    $log->log($_SESSION['name'].': '.$message);        
                    
                }
                else
                {
                    
                    $log->log($_SESSION['name'].': '.$message.$new_bill_id);        
                    
                    $arr_error['error']=0;
                    
                }
                
            }
            else
            {
                
                $arr_error['txt_error']=I18n::lang('phangoapp/tpv', 'error_creating_bill', 'Error: cannot create the new bill -> '.$bill->std_error);
                
                $log->log($_SESSION['name'].': '.$arr_error['txt_error']);
                
            }
            
        }
        
        $arr_error['csrf_token']=PhangoApp\PhaUtils\Utils::generate_csrf_key();
        
        header('Content-type: application/json');
        
        echo json_encode($arr_error);
        
        die;
        
        
    }
    
    public function get_pending_orders() 
    {
     
        $arr_data=['error' => 0];
        
        $bill=new Bill();
        
        $arr_data['bills']=$bill->where(['where payment=? and cancelled=?', [0, 0]])->select_to_list(['id', 'table_id', 'total_price']);
     
        header('Content-type: application/json');
        
        echo json_encode($arr_data);
        
        die;
        
    }
    
    public function make_payment()
    {
        
        settype($_GET['payment_id'], 'integer');
        settype($_GET['money_paid'], 'integer');
        
        $product=new Products();
        
        $product->register('units_solded', new PhangoApp\PhaModels\CoreFields\CharField());
                
        $product->components['units_solded']->quot_open='';
        $product->components['units_solded']->quot_close='';
        
        $product->create_forms();
        
        $product->reset_require();
        
        $bill=new Bill();
        $bill->create_forms();
        $log=new LogTpv();

        if($bill->where(['where id=? and cancelled=? and enterprise_id=?', [$_GET['payment_id'], 0, $_SESSION['enterprise_id']]])->update(['payment' => 1, 'money_paid' => $_GET['money_paid'] ]))
        {
        
            $product->where(['where id IN (select product_id from productbill where bill_id=?)', [$_GET['payment_id']]])->update(['units_solded' => 'units_solded+1']);
            
            $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'bill_paid', 'Bill payed nº ').$_GET['payment_id']);
            
            $arr_data=['error' => 0];
            
            header('Content-type: application/json');
            
            echo json_encode($arr_data);
            
            die;
            
        }
        else
        {
            
            $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'error_bill_paid', 'Error Bill payment nº ').$_GET['payment_id'].' '.$bill->std_error);
            
            echo $bill->std_error;
            
        }
        
    }
    
    public function get_order()
    {
        
        settype($_GET['bill_id'], 'integer');
        
        $bill=new Bill();
        
        $productbill=new ProductBill();
        
        $arr_data=['error' => 0];
        
        $arr_bill=$bill->select_a_row($_GET['bill_id'], ['id', 'table_id'], true);
        
        $arr_data['bill_id']=$arr_bill['id'];
        $arr_data['table_id']=$arr_bill['table_id'];
        
        $arr_data['products']=$productbill->where(['where bill_id=?', [$_GET['bill_id']]])->select_to_list();
            
        header('Content-type: application/json');
        
        echo json_encode($arr_data);
        
        die;
        
    }
    
    public function ticket_print()
    {
        
        settype($_GET['bill_id'], 'integer');
        
        $enterprise=new Enterprise();
        
        $arr_enterprise=$enterprise->select_a_row($_SESSION['enterprise_id']);
        
        $bill=new Bill();
        
        $productbill=new ProductBill();
        
        $arr_data=['error' => 0];
        
        $arr_bill=$bill->select_a_row($_GET['bill_id'], [], true);
        
        $arr_data['bill_id']=$arr_bill['id'];
        $arr_data['table_id']=$arr_bill['table_id'];
        
        $arr_data['products']=$productbill->where(['where bill_id=?', [$_GET['bill_id']]])->select_to_list();
        
        $log=new LogTpv();
        
        $log->log($_SESSION['name'].': '.I18n::lang('phangoapp/tpv', 'print_ticket', 'Print bill ticket nº').' '.$_GET['bill_id']);
        
        echo View::load_view([$arr_enterprise, $arr_bill, $arr_data], 'tpv/ticket');
        
    }

}

?>
