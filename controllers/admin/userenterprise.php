<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function UserEnterpriseAdmin()
{

    settype($_GET['parent'], 'integer');
    
    $parent=$_GET['parent'];

    $categories=new CategoryProduct();

    $products=new Products();
    
    $products->components['category_id']->form='PhangoApp\PhaModels\Forms\SelectModelFormByOrder';
    
    $products->components['category_id']->parameters=[$categories, 'name', 'parent', ['WHERE 1=1', []], 1];
    
    $products->create_forms();
    
    $products->forms['category_id']->default_value=$parent;
    
    $admin=new GenerateAdminClass($products, AdminUtils::set_admin_link('tpv/products'));
    
    $admin->list->arr_fields_showed=['name', 'image'];
    
    if($parent>0)
    {
    
        $admin->list->where_sql=['WHERE (category_id=? OR category_id IN (select id from categoryproduct where parent=?))', [$parent, $parent]];
        
    }
    
    echo View::load_view([$admin, $parent], 'tpv/products', 'phangoapp/tpv');

}

?>
