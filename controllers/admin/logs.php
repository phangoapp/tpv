<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function LogsAdmin()
{
    
    settype($_GET['op'], 'integer');
    
    $log=new LogTpv();
    
    switch($_GET['op'])
    {
        
        default:
        
            settype($_GET['enterprise_id'], 'integer');
    
            $enterprise_id=$_GET['enterprise_id'];
    
            $enterprise=new Enterprise();
            
            $log->components['enterprise_id']->form='PhangoApp\PhaModels\Forms\HiddenForm';
    
            //$log->components['enterprise_id']->parameters=[$enterprise, 'name', 'id', ['WHERE 1=1', []], 1];
    
            $log->components['enterprise_id']->default_value=$enterprise_id;
    
            $log->create_forms();
    
            $admin=new GenerateAdminClass($log, AdminUtils::set_admin_link('tpv/logs', ['enterprise_id' => $enterprise_id]));
            
            $admin->list->yes_options=false;
            
            $admin->list->yes_search=true;
            
            $admin->no_insert=true;
                
            $admin->no_delete=true;
            
            $admin->list->arr_fields_search=['id'];
            
            $admin->list->order_field='date';
            
            $admin->list->order=1;
            
            /*$admin->admin_view='tpv/adminlist';
            $admin->admin_module='phangoapp/tpv';
            
            $admin->arr_fields_edit=['name', 'enterprise_id'];
            
            $admin->list->options_func='enterprise_options';
            
            $admin->list->arr_extra_fields=[I18n::lang('phangpapp/tpv', 'default_table', 'Default table')];
            $admin->list->arr_extra_fields_func=['default_table_options'];*/
            
            echo '<p></p>';
            
            $admin->list->where_sql=['WHERE enterprise_id=?', [$enterprise_id]];
                
            echo View::load_view([$admin, $enterprise_id], 'tpv/logs', 'phangoapp/tpv');
            
        break;
        

        
    }

}
/*
function default_table_options($arr_row)
{
    $checked='';
    
    if($arr_row['default']==1)
    {
        
        $checked=' checked';
        
    }
    
    return '<input type="radio" name="principal_table" class="radio_button" value="'.$arr_row['id'].'" '.$checked.'/>';
    
}*/

/*
function enterprise_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=SimpleList::BasicOptionsListModel($url_options, $model_name, $id);
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/workers', ['enterprise_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'workers_of_this', 'Enterprise\'s Workers').'</a>';
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/stock', ['enterprise_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'stock', 'Stock').'</a>';
    
    return $arr_options;
    
}*/

?>
