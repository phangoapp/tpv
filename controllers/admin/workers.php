<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function WorkersAdmin()
{
    settype($_GET['enterprise_id'], 'integer');
    
    $enterprise_id=$_GET['enterprise_id'];
    
    $userenterprise=new UserEnterprise();
    
    $enterprise=new Enterprise();
    
    $userenterprise->components['enterprise_id']->form='PhangoApp\PhaModels\Forms\SelectModelForm';
    
    $userenterprise->components['enterprise_id']->parameters=[$enterprise, 'name', 'id', ['WHERE 1=1', []], 1];
    
    $userenterprise->create_forms();
    
    $repeat_password=new \PhangoApp\PhaModels\Forms\PasswordForm('repeat_password');
        
    $repeat_password->label=I18n::lang('users', 'repeat_password', 'Repeat password');
    $repeat_password->required=1;
    
    $userenterprise->insert_after_field_form('password', 'repeat_password', $repeat_password);
    
    $admin=new GenerateAdminClass($userenterprise, AdminUtils::set_admin_link('tpv/workers'));
    
    $admin->list->arr_fields_showed=['username', 'name', 'email'];
    
    //$admin->list->options_func='enterprise_options';
    
    if($enterprise_id>0)
    {
    
        $admin->list->where_sql=['WHERE enterprise_id=?', [$enterprise_id]];
        
    }
    
    echo View::load_view([$admin, $enterprise_id], 'tpv/workers', 'phangoapp/tpv');

}
/*
function enterprise_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=SimpleList::BasicOptionsListModel($url_options, $model_name, $id);
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/workers', ['enterprise_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'workers_of_this', 'Enterprise\'s Workers').'</a>';
    
    return $arr_options;
    
}
*/

?>
