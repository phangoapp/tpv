<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function StockAdmin()
{
    
    settype($_GET['op'], 'integer');
    
    settype($_GET['product_id'], 'integer');

    $product_id=$_GET['product_id'];
    
    settype($_GET['enterprise_id'], 'integer');

    $enterprise_id=$_GET['enterprise_id'];

    $product=new Products();

    $stock=new Stock();
    
    $stock->components['enterprise_id']->form='PhangoApp\PhaModels\Forms\HiddenForm';
    $stock->components['product_id']->form='PhangoApp\PhaModels\Forms\HiddenForm';
    
    $stock->create_forms();
    
    $enterprise=new Enterprise();
    
    switch($_GET['op'])
    {
        
        default:
        
            $arr_product=$product->select_a_row($product_id);
            
            if($arr_product)
            {
        
                echo View::load_view([$arr_product, $stock, $enterprise, $enterprise_id], 'tpv/stock', 'phangoapp/tpv');
                
            }
            
        break;
        
        case 1:
        
            AdminUtils::$show_admin_view=false;
            
            $arr_stock=$stock->where(['where enterprise_id=? and product_id=?', [$enterprise_id, $product_id]])->select_a_row_where(['avaliable'], true);
            
            $arr_avaliable['avaliable']=0;
            
            if($arr_stock)
            {
                
                $arr_avaliable['avaliable']=$arr_stock['avaliable'];
                
            }
            
            header('Content-type: application/json');
            
            echo json_encode($arr_avaliable);
            
            die;
        
        break;
        
        case 2:
            
            AdminUtils::$show_admin_view=false;
            
            settype($_POST['product_id'], 'integer');

            $product_id=$_POST['product_id'];
            
            settype($_POST['enterprise_id'], 'integer');

            $enterprise_id=$_POST['enterprise_id'];
            
            settype($_POST['avaliable'], 'integer');
            
            $avaliable=$_POST['avaliable'];
            
            $insert='insert';
            
            if($stock->where(['where enterprise_id=? and product_id=?', [$enterprise_id, $product_id]])->select_count()>0)
            {
                
                $insert='update';
                
            }
            
            $arr_error=['error' => 1, 'form' => []];
            
            if($stock->where(['where enterprise_id=? and product_id=?', [$enterprise_id, $product_id]])->$insert(['enterprise_id' => $enterprise_id, 'product_id'  => $product_id, 'avaliable' => $avaliable]))
            {
                
                $arr_error['error']=0;
                
            }
            
            header('Content-type: application/json');
            
            echo json_encode($arr_error);
        
            die;
        
        break;
        
    }
    
    //$arr_stock=
    
    /*
    $admin=new GenerateAdminClass($stock, AdminUtils::set_admin_link('tpv/stock'));
    
    $admin->list->yes_options=0;
    
    $admin->list->arr_fields_showed=['product_id', 'enterprise_id'];
    
    $admin->list->arr_extra_fields=[I18n::lang('phangoapp/tpv', 'units', 'Units')];
    $admin->list->arr_extra_fields_func=['unit_options'];
    
    echo View::load_view([$admin, $product_id], 'tpv/stock', 'phangoapp/tpv');
    */
    

}

function unit_options($arr_row)
{
    
    
    return '<input type="text" size="2" name="units['.$arr_row['id'].']" maxlength="5" value=""/>';
    
}

?>
