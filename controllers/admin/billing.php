<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaModels\CoreFields\MoneyField;
use PhangoApp\PhaModels\CoreFields\SumField;
use PhangoApp\PhaModels\Forms\SelectModelForm;
use PhangoApp\PhaTime\DateTimeNow;
use PhangoApp\PhaTime\DateTime;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function BillsAdmin() 
{
    $bill=new Bill();
    
    $now=DateTimeNow::$today_first;
    
    settype($_GET['op'], 'integer');
    settype($_GET['begin_page'], 'integer');
    settype($_GET['enterprise_id'], 'integer');
    
    $enterprise_id=$_GET['enterprise_id'];
    
    $sql_enterprise='';
    
    if($enterprise_id>0)
    {
        
        $sql_enterprise=' AND bill.enterprise_id='.$enterprise_id;
        
    }
    
    $months=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    
    $form_category=new SelectModelForm('enterprise_id', $enterprise_id, new Enterprise(), 'name', 'id', ['WHERE 1=1', []], 1);
    
    $form_category->default_value=$enterprise_id;
    
    ?>
    <form method="get" action="<?php echo PhangoApp\PhaLibs\AdminUtils::set_admin_link('billing/bills', ['op' => $_GET['op']]); ?>">
        <p>Elegir empresa: <?php echo $form_category->form(); ?> <input type="submit" value="<?php echo PhangoApp\PhaI18n\I18n::lang('phangoapp\tpv', 'change_enterprise', 'Change enterprise'); ?>" /></p>
    </form>
    <?php
    
    switch($_GET['op'])
    {
        
        default:
    
            $q=$bill->execute('select SUM(total_price),SUM(neto_price), SUM(tax_price)  from bill where payment=1'.$sql_enterprise, []);
            
            list($total_price, $total_price_neto, $total_price_tax)=$bill->fetch_row($q);
            
            $total_price=MoneyField::currency_format($total_price);

            $total_price_neto=MoneyField::currency_format($total_price_neto);

            $total_price_tax=MoneyField::currency_format($total_price_tax);

            $q=$bill->execute('select SUM(total_price) from bill where payment=1 AND date LIKE ?'.$sql_enterprise, [substr($now, 0, 6).'%']);
            
            $total_price_month=MoneyField::currency_format($bill->fetch_row($q)[0]);
            
            $q=$bill->execute('select SUM(total_price) from bill where payment=1 AND date LIKE ?'.$sql_enterprise, [substr($now, 0, 8).'%']);
            
            $total_price_day=MoneyField::currency_format($bill->fetch_row($q)[0]);
            
            $arr_list=[];
            
            $total_months=$bill->executed('select count(DISTINCT month) from bill where payment=1'.$sql_enterprise, [])->fetch_row()[0];
            
            $arr_month=[];
            
            //$total_months=$q->fetch_row()[0];
            
            $q=$bill->executed('select month, SUM(total_price) from bill WHERE payment=1 '.$sql_enterprise.' group by month order by month DESC limit ?, 12', [$_GET['begin_page']]);
            
            while($month=$q->fetch_row())
            {
                
                $month[1]=MoneyField::currency_format($month[1]);
                
                $arr_month[]=$month;
                
            }
            
            //Get the first bill register and last, and get all months. Get last 12 months. Get registers of 12 last monts. 
            
            //$arr_month=
            
            //listing with bills by month. search by month. 
            
            //Download how csv. All, number of monts, all months.
            
            //echo MoneyField::currency_format($total_price);
            
            echo View::load_view([$total_price, $total_price_neto, $total_price_tax, $total_price_month, $total_price_day, $arr_month, $total_months], 'tpv/billing', 'phangoapp/tpv');
            
        break;
        
        case 1:

            settype($_GET['month'], 'integer');
            
            //echo DateTime::format_date('20170122230420');
            
            $bill->components['total_price']->operation='SUM';
            
            $bill->components['neto_price']->operation='SUM';
            
            $bill->components['tax_price']->operation='SUM';

            $bill->set_group(['openbox_id' => 1]);

            $admin=new GenerateAdminClass($bill, AdminUtils::set_admin_link('billing/bills', ['op' => 1]));
            
            $admin->list->options_func='month_options';
            
            $admin->list->where_sql=['where month='.$_GET['month'].' and payment=1'.$sql_enterprise, []];
            
            $admin->list->arr_fields_showed=['openbox_id', 'total_price', 'neto_price', 'tax_name', 'tax_price', 'discount_price', 'payment'];
            
            $nmonth=substr($_GET['month'], 4, 2);
            
            settype($nmonth, 'integer');
            
            if(!isset($months[$nmonth-1]))
            {
                
                $months[$nmonth-1]=0;
                
            }
            
            $month=$months[$nmonth-1];
            
            echo View::load_view([$admin, $month], 'tpv/billingmonth', 'phangoapp/tpv');
            
        break;
        
        case 2:
        
            $openbox=new OpenBox();
        
            settype($_GET['openbox_id'], 'integer');
            
            $arr_openbox=$openbox->select_a_row($_GET['openbox_id']);
            
            if($arr_openbox)
            {
            
                $admin=new GenerateAdminClass($bill, AdminUtils::set_admin_link('billing/bills', ['op' => 2, 'openbox_id' => $arr_openbox['id']]));
                
                $admin->list->raw_query=false;
                
                $admin->list->yes_search=1;
                
                $admin->list->arr_fields_search=['id', 'total_price'];
                
                $admin->list->order=1;
                
                $admin->list->where_sql=['where openbox_id=?'.$sql_enterprise, [$arr_openbox['id']]];
                
                $admin->list->arr_fields_showed=['id', 'total_price', 'neto_price', 'tax_name', 'tax_price', 'discount_price', 'payment'];
                
                $admin->list->options_func='day_options';                
                
                //$rest = substr("abcdef", -3, 1); // devuelve "d"
                
                $nmonth=substr($arr_openbox['date'], -10, 2);
                
                settype($nmonth, 'integer');
                
                $month=$months[$nmonth-1];
                
                echo View::load_view([$admin, $month, DateTime::format_date($arr_openbox['date']), $arr_openbox['date']], 'tpv/billingday', 'phangoapp/tpv');
            }

        
        break;
        
        case 3:
        
            $productbill=new ProductBill();
            
            settype($_GET['bill_id'], 'integer');
            
            $arr_bill=$bill->select_a_row($_GET['bill_id']);
            
            if($arr_bill)
            {
                
                
                $admin=new GenerateAdminClass($productbill, '');
                
                $admin->list->where_sql=['WHERE bill_id=?', [$_GET['bill_id']]];
                
                $admin->list->arr_fields_showed=['name', 'units', 'price', 'discount_percent', 'discount_price', 'total_price_discount', 'tax_name', 'tax_percent', 'tax_price', 'total_price_with_all'];
                
                $admin->show();
                
            }
            
        
        break;
        
    }
    
}

function day_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=[];
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('billing/bills', ['op' => 3, 'bill_id' => $id, 'enterprise_id' => $_GET['enterprise_id']]).'">'.I18n::lang('phangoapp/tpv','view_bill', 'View bill').'</a>';
    
    $arr_options[]='<a href="">'.I18n::lang('phangoapp/tpv','print_bill', 'Print bill').'</a>';
    
    return $arr_options;
    
}

function month_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=[];
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('billing/bills', ['op' => 2, 'openbox_id' => $arr_row['openbox_id_id'], 'enterprise_id' => $_GET['enterprise_id']]).'">'.I18n::lang('phangoapp/tpv','view_all_bills_this_day', 'View bills of this day').'</a>';
    
    return $arr_options;
    
}

?>
