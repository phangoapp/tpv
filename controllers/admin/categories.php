<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function CategoriesAdmin()
{
    settype($_GET['parent'], 'integer');
    
    $parent=$_GET['parent'];

    $taxes=new Taxes();

    $categories=new CategoryProduct();
    
    $categories->components['parent']->form='PhangoApp\PhaModels\Forms\SelectModelFormByOrder';
    
    $categories->components['parent']->parameters=[$categories, 'name', 'parent', ['WHERE 1=1', []], 1];
    
    $categories->components['tax_id']->form='PhangoApp\PhaModels\Forms\SelectModelForm';
    
    $categories->components['tax_id']->parameters=[$taxes, 'name', 'id'];
    
    $categories->create_forms();
    
    $categories->forms['parent']->default_value=$parent;

    $arr_parent=$categories->select_a_row($parent);
    
    if($arr_parent)
    {
        
        $categories->forms['tax_id']->default_value=$arr_parent['tax_id'];
        
    }

    $admin=new GenerateAdminClass($categories, AdminUtils::set_admin_link('tpv/categories', ['parent' => $parent]));
    
    $admin->list->arr_fields_showed=['name'];
    
    $admin->list->yes_search=1;
    
    $arr_cat=[];
        
    $admin->list->where_sql=['WHERE parent=?', [$parent]];
        
    $arr_cat=$categories->select_a_row($parent);
    
    $menu=new ParentLinks(AdminUtils::set_admin_link('tpv/categories'), $categories, 'parent', 'name', $parent, 0, $arr_parameters=[], $arr_pretty_parameters=[]);
    
    echo View::load_view([$admin, $menu, $parent], 'tpv/categories', 'phangoapp/tpv');

}

?>
