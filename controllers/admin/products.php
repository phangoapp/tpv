<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;


Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function ProductsAdmin()
{

    settype($_GET['parent'], 'integer');
    settype($_GET['op'], 'integer');
    
    $parent=$_GET['parent'];

    $categories=new CategoryProduct();
    
    $arr_category=$categories->select_a_row($parent);

    $taxes=new Taxes();

    $products=new Products();
    
    switch($_GET['op'])
    {
        default:
    
            $products->components['category_id']->form='PhangoApp\PhaModels\Forms\SelectModelFormByOrder';
            
            $products->components['category_id']->parameters=[$categories, 'name', 'parent', ['WHERE 1=1', []], 1];
            
            $products->components['category_id']->default_value=$parent;
            
            $products->components['tax_id']->form='PhangoApp\PhaModels\Forms\SelectModelForm';
            
            $products->components['tax_id']->parameters=[$taxes, 'name', 'id'];
            
            $products->create_forms();
            
            $products->forms['category_id']->default_value=$parent;
            
            if($arr_category)
            {
            
                $products->forms['tax_id']->default_value=$arr_category['tax_id'];
                
            }
            
            $admin=new GenerateAdminClass($products, AdminUtils::set_admin_link('tpv/products', ['parent' => $parent]));
            
            $admin->arr_fields_edit=['name', 'base_price', 'image', 'category_id', 'tax_id'];
            
            $admin->list->arr_fields_showed=['name', 'image', 'base_price'];
            
            $admin->list->options_func='product_options';
            
            if($parent>0)
            {
            
                $admin->list->where_sql=['WHERE (category_id=? OR category_id IN (select id from categoryproduct where parent=?))', [$parent, $parent]];
                
            }
            
            echo View::load_view([$admin, $parent], 'tpv/products', 'phangoapp/tpv');
        
        break;
        
        case 1:
        
            settype($_GET['product_id'], 'integer');
        
            $arr_product=$products->select_a_row($_GET['product_id']);
            
            if($arr_product)
            {
        
                echo View::load_view([$arr_product], 'tpv/taxproducts', 'phangoapp/tpv');
                
            }
        
        break;
        
        case 2:
        
            settype($_GET['product_id'], 'integer');
            
            settype($_POST['neto_price'], 'double');
            settype($_POST['tax_percent'], 'double');
            
            $arr_tax_applied['neto_price']=$_POST['neto_price'];
            $arr_tax_applied['tax_percent']=$_POST['tax_percent'];

            $products->execute('update products set tax_applied=?', [json_encode($arr_tax_applied)]);
            
            View::set_flash(I18n::lang('phangoapp/tpv', 'changed_tax', 'Changed tax'));
            
            header('Location: '.AdminUtils::set_admin_link('tpv/products', ['op' => 1, 'product_id' => $_GET['product_id']]));
            
            die;
        
        break;
        
        case 3:
        
            settype($_GET['product_id'], 'integer');
        
            $arr_product=$products->select_a_row($_GET['product_id']);
            
            if($arr_product)
            {
        
                echo View::load_view([$arr_product], 'tpv/discountsproducts', 'phangoapp/tpv');
                
            }
        
        break;
        
        case 4:
        
            settype($_GET['product_id'], 'integer');
            
            foreach($_POST['discount'] as $percent => $discount)
            {
                // [discount] => 0 [neto_price] => 2.89 [tax_percent] => 
;
                $arr_discounts_applied[$percent]['discount']=round($discount, 2);
                $arr_discounts_applied[$percent]['neto_price']=round($_POST['neto_price_discount'][$percent], 2);
                $arr_discounts_applied[$percent]['tax_percent']=round($_POST['tax_percent_discount'][$percent], 2);
            }
            
            $products->execute('update products set discounts_applied=?', [json_encode($arr_discounts_applied)]);
            
            View::set_flash(I18n::lang('phangoapp/tpv', 'discount_changed', 'Discount changed'));
            
            header('Location: '.AdminUtils::set_admin_link('tpv/products', ['op' => 3, 'product_id' => $_GET['product_id']]));
            
            die;
        
        break;
        
        
    }

}

function product_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=SimpleList::BasicOptionsListModel($url_options, $model_name, $id);
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/stock', ['product_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'stock', 'Stock').'</a>';
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/products', ['op' => 1, 'product_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'tax_applied', 'Tax applied').'</a>';
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/products', ['op' => 3, 'product_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'discounts_applied', 'Discounts applied').'</a>';
    
    return $arr_options;
    
}

?>
