<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaView\View;
use PhangoApp\PhaLibs\GenerateAdminClass;
use PhangoApp\PhaLibs\SimpleList;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaLibs\ParentLinks;
use PhangoApp\PhaI18n\I18n;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

function EnterprisesAdmin()
{
    
    $enterprise=new Enterprise();
    
    $enterprise->components['footer_ticket']->form='PhangoApp\PhaModels\Forms\TextAreaForm';
    
    $admin=new GenerateAdminClass($enterprise, AdminUtils::set_admin_link('tpv/enterprises'));
    
    $admin->list->options_func='enterprise_options';
    
    echo '<p></p>';
    
    $admin->show();

}

function enterprise_options($url_options, $model_name, $id, $arr_row)
{
    
    $arr_options=SimpleList::BasicOptionsListModel($url_options, $model_name, $id);
    
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/workers', ['enterprise_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'workers_of_this', 'Enterprise\'s Workers').'</a>';
    $arr_options[]='<a href="'.AdminUtils::set_admin_link('tpv/stock', ['enterprise_id' => $id]).'">'.I18n::lang('phangoapp/tpv', 'stock', 'Stock').'</a>';
    
    return $arr_options;
    
}

?>
