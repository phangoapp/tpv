<?php

use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaRouter\Controller;
use PhangoApp\PhaView\View;
use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaModels\ModelForm;
use PhangoApp\PhaUtils\Utils;

Webmodel::load_model('vendor/phangoapp/tpv/models/products');

class LoginController extends Controller {

    public function home()
    {
        $userenterprise=new UserEnterprise();

        if(!isset($_COOKIE['tpv_cookie']))
        {
            
            //$form_enterprise=new Forms\SelectModelForm('enterprise_id_selected', $enterprise_id, new Enterprise(), 'name', 'id', ['WHERE 1=1 order by name ASC', []], 1);

            $userenterprise->components['enterprise_id']->form='PhangoApp\PhaModels\Forms\SelectModelForm';
            $userenterprise->components['enterprise_id']->parameters=[new Enterprise(), 'name', 'id', ['WHERE 1=1 order by name ASC', []]];


            $userenterprise->create_forms();
            
            $userenterprise->forms['enterprise_id']->label=PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'enterprise', 'Enterprise');

            $forms=ModelForm::show_form($userenterprise->forms, [], $pass_values=false, $check_values=false, $keys_form=['enterprise_id', 'email', 'password']);
        
            echo View::load_view([$forms], 'tpv/login');
            
        }
        else
        {
            
            $arr_user=$userenterprise->where(['WHERE login_token=?', [$_COOKIE['tpv_cookie']]])->select_a_row_where();
            
            if($arr_user)
            {
                
                //Renew the cookie
                set_login($arr_user, 1, $userenterprise);
                
                header('Location: '.PhangoApp\PhaRouter\Routes::get_url('tpv'));
        
                die;
                
            }
            
        }
        
    
    }
    
    public function login()
    {
        
        $arr_error['error']=1;
        
        settype($_POST['email'], 'string');
        settype($_POST['password'], 'string');
        settype($_POST['enterprise_id'], 'integer');
        settype($_POST['remember_login'], 'integer');
        
        $userenterprise=new UserEnterprise();
        
        $arr_row=$userenterprise->where(['where enterprise_id=? and email=?', [$_POST['enterprise_id'], $_POST['email']]])->select_a_row_where([], true);
        
        if($arr_row)
        {
            
            if(PhangoApp\PhaModels\CoreFields\PasswordField::check_password($_POST['password'], $arr_row['password']))
            {
                
                $arr_error['error']=0;
                
                set_login($arr_row, $_POST['remember_login'], $userenterprise);
                
            }
            else
            {
                
                $arr_error['csrf_token']=PhangoApp\PhaUtils\Utils::generate_csrf_key();
                
            }
            
        }
        else
        {
            
            $arr_error['csrf_token']=PhangoApp\PhaUtils\Utils::generate_csrf_key();
            
        }
        
        header('Content-type: application/json');
        
        echo json_encode($arr_error);
        
        die;
        
    }
    
    public function logout()
    {
        
        $_SESSION=[];
        
        session_regenerate_id(true);
        
        setcookie('tpv_cookie', '', time()-3600, Routes::$root_url);
        
        header('Location: '.PhangoApp\PhaRouter\Routes::get_url('tpv/login'));
        
        die;
        
    }

}

function set_login($arr_row, $remember_login, $userenterprise)
{
    
    $_SESSION['login_tpv']=1;
    $_SESSION['id']=$arr_row['id'];
    $_SESSION['enterprise_id']=$arr_row['enterprise_id'];
    $_SESSION['name']=$arr_row['name'];
    
    if($remember_login)
    {
        
        $lifetime=time()+315360000;
        
        $new_token=sha1(Utils::get_token());
        
        //Update
        
        $userenterprise->reset_require();
        
        $userenterprise->fields_to_update=['login_token'];
        
        if($userenterprise->where(['WHERE id=?', [$arr_row['id']]])->update(['login_token' => $new_token]))
        {
        
            setcookie('tpv_cookie', $new_token, $lifetime, Routes::$root_url);
            
        }
        
    }
    
}

?>
