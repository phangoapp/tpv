<?php

PhangoApp\PhaI18n\I18n::load_lang('phangoapp/tpv');

ModuleAdmin::$arr_modules_admin[]=['tpv', 
[
array('tpv/categories', 'vendor/phangoapp/tpv/controllers/admin/categories', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'categories', 'Categories')),
array('tpv/products', 'vendor/phangoapp/tpv/controllers/admin/products', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'products', 'Products')),
array('tpv/enterprises', 'vendor/phangoapp/tpv/controllers/admin/enterprises', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'enterprises', 'Enterprises')),
array('tpv/workers', 'vendor/phangoapp/tpv/controllers/admin/workers', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'workers', 'Workers')),
array('tpv/stock', 'vendor/phangoapp/tpv/controllers/admin/stock', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'stock', 'Stock'), ''),
array('tpv/tables', 'vendor/phangoapp/tpv/controllers/admin/tables', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'tables', 'Tables')),
array('tpv/logs', 'vendor/phangoapp/tpv/controllers/admin/logs', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'logs', 'Logs')),

], 
PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'tpv', 'TPV')];

ModuleAdmin::$arr_modules_admin[]=['billing', 
[
array('billing/taxes', 'vendor/phangoapp/tpv/controllers/admin/taxes', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'taxes', 'Taxes')),
array('billing/bills', 'vendor/phangoapp/tpv/controllers/admin/billing', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'billing', 'Billing')),
array('billing/expenses', 'vendor/phangoapp/tpv/controllers/admin/expenses', PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'expenses', 'Expenses'))

],
PhangoApp\PhaI18n\I18n::lang('phangoapp/tpv', 'billing', 'Billing')];

?>
