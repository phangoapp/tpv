<?php

use PhangoApp\PhaModels\Forms;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaModels\Forms\MoneyForm;

function TaxProductsView($arr_product)
{
    
    ?>
    <p><a href="<?php echo AdminUtils::set_admin_link('tpv/products'); ?>"><?php echo I18n::lang('phangoapp/tpv', 'products', 'Products'); ?></a> &gt;&gt; <?php echo $arr_product['name']; ?></p>
    <h2><?php echo I18n::lang('phangoapp/tpv', 'tax_of_product', 'Tax of product'); ?></h2>
    <p><?php echo I18n::lang('phangoapp/tpv', 'adjust_tax_percent', 'If you need ajust the round of tax, use this formulary'); ?></p>
    <form method="post" class="form" action="<?php echo AdminUtils::set_admin_link('tpv/products', ['op' => 2, 'product_id' => $arr_product['id']]); ?>">
        <?php
        
        $arr_tax=json_decode($arr_product['tax_applied'], true);

        $neto_price=new MoneyForm('neto_price', $arr_tax['neto_price']);
        
        ?><p><label><?php echo I18n::lang('phangoapp/tpv', 'neto_price', 'Neto price'); ?>:</label><?php
        echo $neto_price->form();
        
        ?>
        </p>
        <?php
        
        $tax_percent=new MoneyForm('tax_percent', $arr_tax['tax_percent']);
        
        ?><p></p><label><?php echo I18n::lang('phangoapp/tpv', 'tax_percent', 'Tax percent'); ?>:</label><?php
        echo $tax_percent->form();
        ?>
        <?php echo PhangoApp\PhaUtils\Utils::set_csrf_key($name_token='csrf_token', $length_token=80); ?>
        </p>
        <p><input type="submit" value="<?php echo I18n::lang('phangoapp/tpv', 'round_price', 'Round price'); ?>" /></p>
    </form>
    <?php

}

?>
