<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;

function LoginView($forms)
{

    View::$js[]='jquery.min.js';
    View::$css_module['tpv'][]='login_tpv.css';
    View::$css[]='font-awesome.min.css';

    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <title><?php echo I18n::lang('phangoapp/tpv', 'tpv_login', 'TPV login'); ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <?php echo View::load_css(); ?>
    <?php echo View::load_js(); ?>
    <?php echo View::load_header(); ?> 
    <script language="javascript">
        $(document).ready( function () {
        
            $('#login_submit').click( function () {
            
                $('#loading').show();
                
                data_form={'enterprise_id': $('#enterprise_id_field_form').val(), 'email': $('#email_field_form').val(), 'password': $('#password_field_form').val(), 'csrf_token': $("#csrf_token_field_form").val(), 'remember_login': $('#remember_login').val()};
            
                if($('#remember_login:checked').val())
                {
                    
                    data_form.remember_login=$('#remember_login').val();
                }
            
                $.ajax({
                url: "<?php echo Routes::get_url('tpv/login/login'); ?>",
                method: "POST",
                dataType: "json",
                data: data_form
                }).done(function(data) {
                
                    if(data.error==0)
                    {
                        
                        window.location.href="<?php echo Routes::get_url('tpv'); ?>";
                    
                    }
                    else
                    {
                        
                        // Firefox have a horrible and stupid bug and you need attr for set de new csrf_token
                        
                        $('#csrf_token_field_form').attr('value', data.csrf_token);
                    
                        $('#loading').hide('slow');

                        $('#email_error').html("<?php echo I18n::lang('phangoapp/tpv', 'error_login', 'Error, wrong enterprise, email or password'); ?>");
                        
                    }
                
                }).fail(function(data) {
                    
                    $('#loading').hide('slow');
                    
                    alert('Error: cannot login '+JSON.stringify(data));
                });
                
                return false;
            
            });
        
        });
    </script>
    </head>
    <body>
        <form id="login">
            <div id="title">
                <?php echo I18n::lang('phangoapp/tpv', 'tpv_login', 'TPV Login'); ?>
            </div>
            <?php echo $forms; ?>
            <div id="email_error" class="error form"></div>
            <div class="form">
                <?php echo I18n::lang('phangoapp/tpv', 'remember_login', 'Remember login?'); ?> <input type="checkbox" id="remember_login" name="remember_login" value="1">
            </div>
            <div id="submit_block">
                <input type="submit" value="<?php echo I18n::lang('common', 'login', 'Login'); ?>" class="submit" id="login_submit"/>
                <span id="loading">&nbsp;</span>
            </div>
        </form>
    </body>
</html>

    <?php

}

?>
