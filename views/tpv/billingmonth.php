<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaTime\DateTime;
use PhangoApp\PhaModels\CoreFields\MoneyField;
use PhangoApp\PhaUtils\SimpleTable;
use PhangoApp\PhaUtils\Pages;

function BillingMonthView($admin, $month)
{

    ?>
    <h2><?php echo I18n::lang('phangoapp/tpv', 'bills_of', 'Bills of '); ?><?php echo $month; ?></h2>
    <p><a href="<?php echo AdminUtils::set_admin_link('billing/bills', ['enterprise_id' => $_GET['enterprise_id']]); ?>"><?php echo I18n::lang('phangoapp/tpv', 'bills', 'Bills'); ?></a> &gt;&gt; <?php echo I18n::lang('phangoapp/tpv', 'bills_of', 'Bills of '); ?><?php echo $month; ?></p>
    <?php

    $admin->show();

}

?>
