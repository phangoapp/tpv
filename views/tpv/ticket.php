<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaTime\DateTime;
use PhangoApp\PhaModels\CoreFields\MoneyField;

function TicketView($arr_enterprise, $arr_bill, $arr_data)
{

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Ticket</title>
  </head>
  <body>
      <div id="table_bill">
        <style>
            @page { margin: 0; }
            body {
                
                font-family:sans;
                margin:5px;
                
            }
            
            td {
              
                padding:15px;
                
            }
        </style>
          <table width="100%" class="table_bill" id="table_bill">
            <tr>
                <td width="50%" style="border: solid #000;border-width: 0px 0px 1px 0px;">
                    <?php echo DateTime::format_fulldate($arr_bill['date']); ?> 
                </td>
                <td width="50%" style="text-align:right;border: solid #000;border-width: 0px 0px 1px 0px;">
                    <?php echo I18n::lang('phangoapp/tpv', 'order', 'Order'); ?>: <?php echo $arr_bill['id']; ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding: 5px 15px;border: solid #000;border-width: 0px 0px 1px 0px;">
                    <?php echo $arr_enterprise['bussiness_name']; ?><br />
                    <?php echo $arr_enterprise['name']; ?><br />
                    NIF: <?php echo $arr_enterprise['nif']; ?><br />
                    <?php echo I18n::lang('phangoapp/tpv', 'phone', 'Phone'); ?>: <?php echo $arr_enterprise['phone']; ?>
                </td>
            </tr>
            <tr>
                <td style="border: solid #000;border-width: 0px 0px 1px 0px;font-weight:bold;">
                  <?php echo I18n::lang('phangoapp/tpv', 'units', 'Units'); ?>  
                </td>
                <td style="text-align:right;border: solid #000;border-width: 0px 0px 1px 0px;font-weight:bold;">
                  <?php echo I18n::lang('phangoapp/tpv', 'taxes', 'Taxes'); ?>  
                </td>
            </tr>
            <?php
            
            $total_price=0;
            
            $total_price_tax=0;
            
            foreach($arr_data['products'] as $product)
            {
            
            //Calculate price
            
            $real_price=$product['total_price'];
            
            $tax_percent=$product['tax_percent'];
            
            $tax_name=$product['tax_name'];
            
            $discount_percent=$product['discount_percent'];
            
            if($product['tax_percent']>0)
            {

                $neto_price=floor($real_price/(($tax_percent/100)+1));

                $total_price+=$neto_price;

                $percent_tax=$real_price-$neto_price;
                
                $total_price_tax+=$percent_tax;
                
                $formatted_price=MoneyField::currency_format($neto_price, '');
                
                //Discount
                
                if($product['discount_percent']>0)
                {
                    
                    $percent_discount=($neto_price/100)*$discount_percent;
                    
                    $neto_price=$neto_price-$percent_discount;
                    
                    $final_discount=MoneyField::currency_format($percent_discount, '');
                    
                    $formatted_price=$formatted_price.' <br />'.I18n::lang('phangoapp/tpv', 'discount', 'Discount').' '.$discount_percent.'% '.'<br /> € '.MoneyField::currency_format($percent_discount, '');
                    
                    
                }
                
                $formatted_tax=MoneyField::currency_format($percent_tax, '');
                
                $total_price_row='€ '.$formatted_price;
                $total_tax_row=$tax_name.' '.$tax_percent.'%<br /> € '.$formatted_tax;
            
            }
            else
            {
                
                $formatted_price=MoneyField::currency_format($real_price, '');
                $formatted_tax=0;
                
                $total_price_row='€ '.$formatted_price;
                $total_tax_row='0% € '.$formatted_tax;
                
            }
                
            ?>
            <tr>
                <td><?php echo $product['units']; ?> x <?php echo Utils::wrap_words($product['name'], 5); ?><br /><?php echo $total_price_row; ?></td> 
                <td style="text-align:right;"><?php echo $total_tax_row; ?></td>
            </tr>    
            <?php
            }
            ?>
            <tr>
                <td style="border: solid #000;border-width: 1px 0px 1px 0px;font-weight:bold;">
                  <?php echo I18n::lang('phangoapp/tpv', 'subtotal', 'SubTotal'); ?>  
                </td>
                <td style="text-align:right;border: solid #000;border-width: 1px 0px 1px 0px;font-weight:bold;">
                  <?php echo I18n::lang('phangoapp/tpv', 'total_taxes', 'Total taxes'); ?>  
                </td>
            </tr>
            <tr>
                <td>
                    &euro; <?php echo MoneyField::currency_format($total_price, ''); ?>
                </td>
                <td style="text-align:right;">
                    &euro; <?php echo MoneyField::currency_format($total_price_tax, ''); ?>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="border: solid #000;border-width: 1px 0px 1px 0px;">
                  <span style="font-size:28px;font-weight:bold;"><?php echo I18n::lang('phangoapp/tpv', 'total', 'Total'); ?></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                  <span style="font-size:28px;font-weight:bold;">&euro; <?php echo MoneyField::currency_format($arr_bill['total_price'], ''); ?></span>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="text-align:center;">
                    <?php echo $arr_enterprise['footer_ticket']; ?>
                </td>
            </tr>
            </table>
        </div>
        <input type="button" onclick="printDiv('table_bill')" value="<?php echo I18n::lang('phangoapp/tpv', 'print_ticket', 'Print ticket'); ?>" />
        <script>
            
        function printDiv(divName) 
        {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
            
            window.close();
            
        }
        
        </script>
  </body>
</html>
<?php

}

?>
