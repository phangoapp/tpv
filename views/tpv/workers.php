<?php

use PhangoApp\PhaModels\Forms;

function WorkersView($admin, $enterprise_id)
{
    
    $form_category=new Forms\SelectModelForm('enterprise_id', $enterprise_id, new Enterprise(), 'name', 'id', ['WHERE 1=1', []], 1);
    
    $form_category->default_value=$enterprise_id;
    
    ?>
    <form method="get" action="<?php echo PhangoApp\PhaLibs\AdminUtils::set_admin_link('tpv/workers'); ?>">
        <p>Elegir empresa: <?php echo $form_category->form(); ?> <input type="submit" value="<?php echo PhangoApp\PhaI18n\I18n::lang('phangoapp\tpv', 'change_enterprise', 'Change enterprise'); ?>" /></p>
    </form>
    <?php

    $admin->show();

}

?>
