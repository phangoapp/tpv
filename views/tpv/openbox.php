<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;

function OpenBoxView()
{

    View::$js[]='jquery.min.js';
    View::$css_module['tpv'][]='login_tpv.css';
    View::$css[]='font-awesome.min.css';

    ?>
    <!DOCTYPE html>
    <html>
    <head>
    <title> <?php echo I18n::lang('phangoapp/tpv', 'box_closed', 'Box closed'); ?></title>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    <?php echo View::load_css(); ?>
    <?php echo View::load_js(); ?>
    <?php echo View::load_header(); ?> 
    <script language="javascript">
        $(document).ready( function () {
        
            $('#open_box').click( function () {
        
                $.ajax({
                    type: "GET",
                    url: "<?php echo Routes::get_url('tpv/index/open_box'); ?>",
                    data: {},
                    success: function (data) {
                        
                        $('#loading').show();
                        
                        if(data.error==0)
                        {
                            
                            location.href="<?php echo Routes::get_url('tpv'); ?>";
                            return;
                            
                        }
                        else
                        {
                            $('#loading').hide();                            
                            alert(data.txt_error);
                            return;
                            
                        }
                        
                    },
                    error: function (data) {
                        
                        $('#loading').hide();
                        
                        alert(JSON.stringify(data));
                  
                    },
                    dataType: 'json'
                });
                
            });
        
        });
    </script>
    </head>
    <body>
        <form id="login">
            <div id="title">
                <?php echo I18n::lang('phangoapp/tpv', 'box_closed', 'Box closed'); ?>
            </div>
            <div id="email_error" class="error form"></div>
            <div class="form">
                <?php echo I18n::lang('common', 'open_box_explain', 'The box is closed. You can open a new session in box making click in "Open Box"'); ?>
            </div>
            <div id="submit_block">
                <input type="submit" value="<?php echo I18n::lang('common', 'open_box', 'Open Box'); ?>" class="submit" id="open_box"/>
                <span id="loading">&nbsp;</span>
            </div>
        </form>
    </body>
</html>

    <?php

}

?>
