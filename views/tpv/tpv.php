<?php 

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;

//Colocar como pendiente y que salga en un icono arriba en pendientes.

function TpvView($user, $form_categories, $form_table, $close_box, $total_price, $total_price_raw)
{

View::$js[]='jquery.min.js';
View::$css_module['tpv'][]='style_tpv.css';
View::$css_module['tpv'][]='vex.css';
View::$css_module['tpv'][]='vex-theme-os.css';
View::$js_module['tpv'][]='vex.combined.min.js';
View::$css[]='font-awesome.min.css';

settype($_GET['bill_id'], 'integer');
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
    <title><?php echo I18n::lang('phangoapp/tpv', 'tpv', 'TPV'); ?> - <?php echo $user['enterprise_id_name']; ?></title>
    <?php echo View::load_css(); ?>
    <?php echo View::load_header(); ?> 
  </head>
  <body>
<nav class="navbar">
  <a class="logo_nav" href="<?php echo Routes::get_url('tpv'); ?>">
        <?php echo I18n::lang('phangoapp/tpv', 'tpv', 'TPV'); ?> - <?php echo $user['enterprise_id_name']; ?>
  </a>
  <div id="header_options">
      <?php echo $form_table->form(); ?>
        <a id="total_money_billed" href="<?php echo Routes::get_url('tpv/showtickets'); ?>"><?php echo I18n::lang('phangoapp/tpv', 'total_billed_today', 'Total billed today'); ?>: <span id="total_money_billed_showed"><?php echo $total_price; ?></span><input id="total_money_billed_raw" type="hidden" value="<?php echo $total_price_raw; ?>" /> <i class="fa fa-eye" aria-hidden="true"></i></a>
      <a id="close_box_button" href="<?php echo Routes::get_url('tpv/index/close_box'); ?>">
          <?php echo I18n::lang('phangoapp/tpv', 'close_box_button', 'Close box'); ?> <i class="fa fa-close" aria-hidden="true"></i>
      </a>
     <a href="#" id="show_pending"><?php echo I18n::lang('phangoapp/tpv', 'pending_orders', 'Orders without payment'); ?>:<span id="pending_orders">0</span> <i class="fa fa-exclamation-triangle" style="color: #ff0000;" aria-hidden="true"></i></a> 
    <div id="select_bill">
    </div>
    <a href="<?php echo Routes::get_url('tpv/login/logout'); ?>" id="logout"> <?php echo I18n::lang('phangoapp/tpv', 'logout', 'Logout'); ?> <i class="fa fa-power-off" style="color: #ff0000;" aria-hidden="true"></i></a>
  </div>
</nav>
    <div class="content">
        <div class="column-4x">
            <div id="products_list">
                <div id="product_row" class="product_row" style="display:none;">
                    <span class="name_product_row"></span> x<span class="units_product_row"></span> <span class="total_price_row"></span><input type="hidden" class="total_price_real" value="0"/> <input type="hidden" class="tax_percent" value="0"/><input type="hidden" class="tax_name" value=""/><input type="hidden" class="discount_percent" value=""/>
                </div>
            </div>
            <div id="options_price">
                <span id="options_price_text"><?php echo I18n::lang('phangoapp/tpv', 'check_all_products', 'Check all products'); ?></span> <input type="checkbox" name="all_products" value="1" id="check_all_products"/>
            </div>
            <div id="taxes_price">
                <?php echo I18n::lang('phangoapp/tpv', 'total_price_without_taxes', 'Total price without taxes'); ?>: € <span id="total_price_no_taxes_bill">0,00</span>
                <?php echo I18n::lang('phangoapp/tpv', 'taxes', 'Taxes'); ?>: € <span id="total_taxes_price">0,00</span>
                <br />
                <?php echo I18n::lang('phangoapp/tpv', 'total_discounts', 'Total discounts'); ?>: € <span id="total_discounts">0,00</span>
            </div>
            <div id="final_price">
                <?php echo I18n::lang('phangoapp/tpv', 'total_price', 'Total price'); ?>: € <span id="total_price_bill">0,00</span><input type="hidden" id="total_price_bill_raw" />
            </div>
            <div id="calculator">
                <div class="column_calculator">               
                    <a href="#" id="one_calculator" class="number_calc">1</a>
                    <a href="#" id="two_calculator" class="number_calc">2</a>
                    <a href="#" id="three_calculator" class="number_calc last_number">3</a>
                    <br />
                    <a href="#" id="four_calculator" class="number_calc">4</a>
                    <a href="#" id="five_calculator" class="number_calc">5</a>
                    <a href="#" id="six_calculator" class="number_calc">6</a>
                    <br />
                    <a href="#" id="seven_calculator" class="number_calc">7</a>
                    <a href="#" id="seven_calculator" class="number_calc">8</a>
                    <a href="#" id="seven_calculator" class="number_calc">9</a>
                    <br />
                    <a href="#" id="plus_minus_calculator" class="number_calc">C</a>
                    <a href="#" id="zero_calculator" class="number_calc">0</a>
                    <a href="#" id="point_calculator" class="number_calc">.</a>
                </div>
                <div class="column_buttons_calc">
                    <input type="text" id="calculator_values" class="options_calc" value="0" />
                    <br />
                    <a href="#" id="quantity_calculator" class="options_calc">Quantity</a>
                    <br />
                    <a href="#" id="discount_calculator" class="options_calc">Discount</a>
                    <br />
                    <a href="#" id="enter_calculator" class="options_calc"><i class="fa fa-caret-square-o-left" aria-hidden="true"></i> Next </a>
                    <input type="hidden" id="added_new_value" value="0" />
                </div>
            </div>
        </div>
        <div class="column-8x">
            <a href="#" class="button_product" id="father_button" style="display:none;">
                <span class="price_product"></span>
                <img src="" />
                <span class="name_product"></span>
                <input type="hidden" class="tax_percent" value="0"/><input type="hidden" class="tax_name" value=""/>
            </a>
            <div class="spinner" id="spinner" style="display:none;">
              <div class="rect1"></div>
              <div class="rect2"></div>
              <div class="rect3"></div>
              <div class="rect4"></div>
              <div class="rect5"></div>
            </div>
            <div id="categories_menu">
                <input type="button" name="button_menu" id="categories_menu_button" value="Seleccionar categoría de producto"/>
                <?php echo $form_categories->form(); ?>
                <span id="category_selected"></span>
            </div>
            <div id="products">
            </div>
            <input type="hidden" id="payment_id" value="0"/>
            <?php echo PhangoApp\PhaUtils\Utils::set_csrf_key(); ?>
        </div>
    </div>
    <div id="popup_pay">
        
    </div>
  </body>
    <?php echo View::load_js(); ?>
    <script>vex.defaultOptions.className = 'vex-theme-os'</script>
    <?php
    if($close_box)
    {
    ?>
    <script>
        alert("<?php echo I18n::lang('phangoapp/tpv', 'open_box_warning', 'Sorry, you box have an open session of other day. You can close the old session and open a new session'); ?>");
    </script>
    <?php
        
    }
    ?>
    <script language="javascript">
        
        //total_sin_iva*(percent/100)=iva

        //total_sin_iva=total_con_iva/((percent/100)+1)
        
        function get_products(category_id)
        {
            $('#products').html('');
            $('#spinner').show();
            $("#products").css('opacity', 1);
            $("#products").prop('disabled',false);
            $('#category_id_field_form').hide();
        
            $.ajax({
              type: "GET",
              url: "<?php echo Routes::get_url('tpv/index/get_products'); ?>",
              data: {category_id: category_id},
              success: function (data) {
                  
                $('#spinner').hide();
                  
                if(data.length>0)
                {
                  
                    //$('#products').html('');
                    
                    for(k in data)
                    {
                        
                        new_product=$('#father_button').clone().appendTo('#products');
                        new_product.children('img').attr('src', data[k].image);
                        new_product.children('.name_product').html(data[k].name);
                        new_product.children('.tax_percent').val(data[k].tax_id_percent);
                        new_product.children('.tax_name').val(data[k].tax_id_name);
                        
                        //formatted_price=(data[k].price/100);
                        
                        new_product.children('.price_product').html(data[k].price);
                        new_product.css('display', 'inline-block');
                        new_product.attr('id', 'product_'+data[k].id+'_price_'+data[k].base_price);
                        new_product.attr('title', data[k].name);

                    }
                    
                    $('.button_product').click(function () {
           
                        //Get data
                        
                        product_id=$(this).attr('id').replace(/product_([0-9]+?)_price_.\d*(\.\d+)?/g, '$1');
                        base_price_id=$(this).attr('id').replace(/product_[0-9]+?_price_(.\d*(\.\d+)?)/g, '$1');                       
                        name=$(this).attr('title');
                        tax_name=$(this).children('.tax_name').val();
                        tax_percent=parseInt($(this).children('.tax_percent').val());
                        
                        if($('#product_row_'+product_id).attr('id')==undefined)
                        {
                        
                            if(name.length>20)
                            {
                                
                                 name = jQuery.trim(name).substring(0, 20).split(" ").slice(0, -1).join(" ") + " [...]";
                                
                            }
                            
                            new_row=$('#product_row').clone().appendTo('#products_list');
                            new_row.children('.name_product_row').html(name);
                            new_row.children('.units_product_row').html(1);
                            
                            
                            if(tax_percent>0)
                            {
                                
                                //percent_tax=(base_price_id/100)*tax_percent;
                                neto_price=Math.floor(base_price_id/((tax_percent/100)+1));
                                
                                percent_tax=base_price_id-neto_price;
                                
                                formatted_price=(neto_price/100).toFixed(2).replace('.', ',');
                                
                                formatted_tax=(percent_tax/100).toFixed(2).replace('.', ',');
                                
                                new_row.children('.total_price_row').html('€ '+formatted_price+' '+tax_name+' '+tax_percent+'% € '+formatted_tax);
                                
                            }
                            else
                            {
                                
                                formatted_price=(base_price_id/100).toFixed(2).replace('.', ',');
                                
                                new_row.children('.total_price_row').html('€ '+formatted_price+' ');
                            }
                            
                            new_row.children('.total_price_real').val(base_price_id);
                            new_row.children('.tax_name').val(tax_name);
                            new_row.children('.tax_percent').val(tax_percent);
                            new_row.addClass('product_row_added');
                            
                            new_row.css('display', 'block');
                            new_row.attr('id', 'product_row_'+product_id);
                            
                            $(new_row).click(function () {                       

                                $(this).toggleClass('active_row');
                            
                            });
                            
                        }
                        else
                        {
                           
                           change_row(product_id, 1, true, 0);
                            
                        }
                        
                        //Recalculate final price
                        
                        recalculate_prices();
                        
                        return false;
                        
                    });
                        
                
                }
                else
                {
                    
                    $('#products').html('<div class="error"><?php echo I18n::lang('phangoapp/tpv', 'error_product', 'Error: no products found'); ?></div>');
                    
                }
                  
              },
              fail: function (data) {
                  
              },
              dataType: 'json'
            });
            
        }
        
        $('#quantity_calculator').click( function () {
           
            units=$('#calculator_values').val();
            
            if((units % 1)==0)
            {
             
                $('.active_row').each(function (index) {
                     
                    product_id=$(this).attr('id').replace('product_row_', '');
                    
                    $(this).children('.units_product_row').html(units);

                    change_row(product_id, units, false, 0);
                    
                });
                
                recalculate_prices();
                
                $('#calculator_values').val(0);
                
                document.getElementById('check_all_products').checked=false;
                $('#options_price_text').html("<?php echo I18n::lang('phangoapp/tpv', 'check_all_products', 'Check all products'); ?>");
                
                $('.active_row').toggleClass('active_row');
                
            }
            else
            {
                
                alert('<?php echo I18n::lang('phangoapp/tpv', 'no_accept_decimals', 'Wrong number. Use integers'); ?>');
                
            }
            
            return false;
        });
        
        //Apply discount
        
        $('#discount_calculator').click( function () {
           
            discount_percent=parseInt($('#calculator_values').val());
            
            if(discount_percent<0 || discount_percent>100)
            {
                
                discount_percent=0;
                
            }
            
            if(discount_percent>=0)
            {
                
                $('.active_row').each(function (index) {
                 
                    product_id=$(this).attr('id').replace('product_row_', '');
                    
                    product_row=$(this);
                    
                    product_row.children('.discount_percent').val(discount_percent);
                    
                    units=$(this).children('.units_product_row').html();
                    
                    change_row(product_id, units, false, discount_percent);
                    
                }); 
                
            }
            
            recalculate_prices();
            
            $('#calculator_values').val(0);
            
            document.getElementById('check_all_products').checked=false;
            $('#options_price_text').html("<?php echo I18n::lang('phangoapp/tpv', 'check_all_products', 'Check all products'); ?>");
            
            $('.active_row').toggleClass('active_row');
            
            return false;
            
        });
        
        $('.number_calc').click( function () {
        
            number=$(this).html();
            
            actual_value=$('#calculator_values').val();
            
            if(actual_value!=0)
            {
                number=actual_value+number;
            }
            
            $('#calculator_values').val(number);
            
            return false;
        
        });
        
        $('#plus_minus_calculator').click( function () {
        
            $('#calculator_values').val(0);
            document.getElementById('check_all_products').checked=false;
            $('#options_price_text').html("<?php echo I18n::lang('phangoapp/tpv', 'check_all_products', 'Check all products'); ?>");
            $('.active_row').toggleClass('active_row');
        
            return false;
        
        });
        
        $('#enter_calculator').click( function () {
        
            if($('.product_row_added').length>0)
            {
                
                payment_id=parseInt($('#payment_id').val());
                
                if(payment_id==0)
                {
        
                    /*vex.dialog.open({
                        message: 'Elija la operación que desea realizar:',
                        input: [
                            'Guardar pedido y cobrar posteriormente <input type="radio" id="save_order_no_bill" name="save_order" value="0" />',
                            'Guardar pedido y cobrar <input type="radio" name="save_order" id="save_order_bill" value="1"/>',
                        ].join(''),
                        buttons: [
                            $.extend({}, vex.dialog.buttons.YES, { text: 'Submit' }),
                            $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
                        ],
                        callback: function (data) {
                            if (data) {
                                
                                if(data.save_order==1)
                                {
                                    
                                    //Other dialog 
                                    save_payment(1);
                                    
                                }
                                else
                                {
                                    
                                    //Clean caja
                                    
                                    save_payment(0);
                                    
                                }
                                
                            }
                        }
                    });*/
                    
                    show_options_bill();
                }
                else
                {
                    if($('#added_new_value').val()=='0')
                    {
                        
                        choose_payment();
                        
                    }
                    else
                    {
                        
                        show_options_bill();
                        
                    }
                    

                    
                }
            }
            else
            {
                
                alert('Error: no hay ningún producto en caja');
                
            }
            
            return false;
        
        });
        
        $('#check_all_products').click( function () {
           
            toggle_class=true;
           
            if(!this.checked) 
            {
                
                toggle_class=false;
                $('#options_price_text').html("<?php echo I18n::lang('phangoapp/tpv', 'check_all_products', 'Check all products'); ?>");
               
            }
            else
            {
                
                $('#options_price_text').html("<?php echo I18n::lang('phangoapp/tpv', 'uncheck_all_products', 'Uncheck all products'); ?>");
                
            }
           
            $('.product_row_added').each(function( index ) {
               
                   
                $(this).toggleClass('active_row', toggle_class);
                   
               
            });
            
            return false;
            
        });
        
        $('#categories_menu_button').click( function () {
            
            if($('#category_id_field_form').css('display')=='none')
            {
        
                $('#category_id_field_form').show();
            
                $("#products").prop('disabled',true);
                $("#products").css('opacity', 0.5);
                
            }
            else
            {
                
                $('#category_id_field_form').hide();
                $("#products").css('opacity', 1);
                $("#products").prop('disabled',false);
                
            }
        
            return false;
        
        });
        
        $('#category_id_field_form').change( function () {
        
            name_cat=$( "#category_id_field_form option:selected" ).text().replace(/-/g, '');
            
            $('#category_selected').html(name_cat);
            
            get_products($(this).val());
            
            return false;
        
        });
        
        $('#show_pending').click(function () {
        
            if($('#select_bill').css('display')=='none')
            {
                
                $("#products").css('opacity', 0.5);
                $("#products").prop('disabled',true);
                $('#select_bill').show();
                
            }
            else
            {
                
                $("#products").css('opacity', 1);
                $("#products").prop('disabled',false);
                $('#select_bill').hide();
                
            }
            
            return false;
        
        });
        
        $('#close_box_button').click( function () {
        
            vex.dialog.open({
                message: '¿Cerrar caja?',
                input: '',
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Sí' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'No' })
                ],
                callback: function (data) {

                    if(data)
                    {
                        
                        location.href="<?php echo Routes::get_url('tpv/index/close_box'); ?>";
                        
                    }

                }
            });
            
        
            return false;
        
        });
        
        function show_options_bill()
        {
            
            vex.dialog.open({
                message: 'Elija la operación que desea realizar:',
                input: [
                    'Guardar pedido y cobrar posteriormente <input type="radio" id="save_order_no_bill" name="save_order" value="0" />',
                    'Guardar pedido y cobrar <input type="radio" name="save_order" id="save_order_bill" value="1"/>',
                ].join(''),
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Submit' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
                ],
                callback: function (data) {
                    if (data) {
                        
                        if(data.save_order==1)
                        {
                            
                            //Other dialog 
                            save_payment(1);
                            
                        }
                        else
                        {
                            
                            //Clean caja
                            
                            save_payment(0);
                            
                        }
                        
                    }
                }
            });
            
        }
                
        function change_row(product_id, num_units, add_units, discount_percent)
        {
            
            old_row=$('#product_row_'+product_id);
            
            if(num_units>0)
            {
            
                if(add_units)
                {
                               
                    units=parseInt($('#product_row_'+product_id).children('.units_product_row').html())+num_units;
                    
                }
                else
                {
                    
                    units=num_units;
                    
                }
                
                tax_name=old_row.children('.tax_name').val();
                tax_percent=parseInt(old_row.children('.tax_percent').val());
                                         
                real_price=old_row.children('.total_price_real').val();
                
                new_price=real_price;

                percent_tax=0;
                neto_price=new_price;
                
                if(tax_percent>0)
                {
                    
                    neto_price=Math.round(new_price/((tax_percent/100)+1));

                    percent_tax=new_price-neto_price;
                    
                    /*percent_tax=(new_price/100)*tax_percent;
                    
                    neto_price=new_price-percent_tax;*/
                    
                    formatted_discount='';
                    
                    if(discount_percent==0)
                    {
                        
                        discount_percent=parseInt(old_row.children('.discount_percent').val());
                        
                    }
                    
                    if(discount_percent>0)
                    {
                        
                        //Add percent
                        
                        percent_discount=Math.floor((neto_price/100)*discount_percent);
                        
                        neto_price=neto_price-percent_discount;
                        
                        final_discount=(percent_discount/100).toFixed(2).replace('.', ',');
                        
                        formatted_discount=' <?php echo I18n::lang('phangoapp/tpv', 'discount', 'Discount'); ?> '+discount_percent+'% '+' € '+(percent_discount/100).toFixed(2).replace('.', ',');
                        
                        //Recalculate tax
                        
                        percent_tax=(neto_price/100)*tax_percent;
                    
                        //neto_price=neto_price-percent_tax;
                        
                    }
                    
                    
                    formatted_price=((neto_price*units)/100).toFixed(2).replace('.', ',');
                    
                    formatted_tax=((percent_tax*units)/100).toFixed(2).replace('.', ',');
                    
                    old_row.children('.total_price_row').html('€ '+formatted_price+' '+tax_name+' '+tax_percent+'% € '+formatted_tax+formatted_discount);
                    
                }
                else
                {
                    
                    new_price=new_price*units;
                    
                    formatted_price=((new_price)/100).toFixed(2).replace('.', ',');

                    old_row.children('.total_price_row').html('€ '+formatted_price+' ');
                }

                old_row.children('.units_product_row').html(units);

            }
            else
            {
                
                $(old_row).remove();
                
            }
            
        }
        
        function recalculate_prices()
        {
            
            final_price=0;
            
            final_price_without_tax=0;
            
            final_price_discount=0;
            
            final_tax_price=0;
            
            arr_taxes=[];
            arr_check_taxes={};
                        
            $('.product_row_added').each(function( index ) {
                
                units_row=parseInt($(this).children('.units_product_row').html());
                
                tax_name=$(this).children('.tax_name').val();
                
                tax_percent=parseInt($(this).children('.tax_percent').val());
                
                discount_percent=parseInt($(this).children('.discount_percent').val());
                
                real_price=parseInt($(this).children('.total_price_real').val())

                real_price_units=real_price*units_row;
                
                neto_price=Math.round(real_price/((tax_percent/100)+1))*units_row;

                percent_tax=real_price_units-neto_price;
                
                if(discount_percent>0)
                {
                    
                    percent_discount=Math.round((neto_price/100)*discount_percent);
                
                    neto_price-=percent_discount;
                    
                    percent_tax=(neto_price/100)*tax_percent;
                    
                    final_price_discount+=percent_discount;
                    
                    real_price=neto_price+percent_tax;
                    
                    
                }
                
                final_tax_price+=percent_tax;
                
                final_price_without_tax+=neto_price;
                
                //arr_taxes.push(tax_name+' '+tax_percent+'%');
                arr_check_taxes[tax_percent]=tax_name;
                
                final_price+=real_price;
                
            });
            
            final_text_taxes='';
            
            for(tax_percent in arr_check_taxes)
            {
                
                final_text_taxes+=' '+arr_check_taxes[tax_percent]+' '+tax_percent+'%';
                
            }
            
            final_price=Math.floor(final_price)*units_row;
            
            formatted_final_price_without_tax=((final_price_without_tax)/100).toFixed(2).replace('.', ',');

            formatted_final_price=((final_price)/100).toFixed(2).replace('.', ',');
            
            formatted_final_tax=((final_tax_price)/100).toFixed(2).replace('.', ',')+' '+final_text_taxes;
            
            formatted_final_discount=((final_price_discount)/100).toFixed(2).replace('.', ',');
            
            $('#total_price_no_taxes_bill').html(formatted_final_price_without_tax);
            
            $('#total_taxes_price').html(formatted_final_tax);
            
            $('#total_price_bill').html(formatted_final_price);
            
            $('#total_price_bill_raw').val(final_price);
            
            $('#total_discounts').html(formatted_final_discount);
            
            //Set changed product 
            
            $('#added_new_value').val(1);
            
        }
        
        function save_payment(yes_payment)
        {
            
            payment_id=parseInt($('#payment_id').val());
            
            table_id=parseInt($('#table_id_field_form').val());
            
            if(table_id==0 || isNaN(table_id))
            {
                
                alert('Error: necesita elegir una mesa');
                
                return false;
                
            }
                
            data=get_data_products();
                
            set_save_products(payment_id, yes_payment, data);
            
        }
        
        function get_data_products()
        {
            
            data={'csrf_token': $('#csrf_token_field_form').val(), 'money_paid': 0, 'table_id': table_id};
                
            data['products']=[];
            
            data['total_price']=parseInt($('#total_price_bill_raw').val())/100;
            
            final_neto_price=0;
            
            total_tax_name=[];
            total_tax_name_dict={};
            
            final_tax_price=0;
            
            //Get data 
            
            $('.product_row_added').each(function( index ) {
                
                product_id=$(this).attr('id').replace('product_row_', '');
            
                name_product=$(this).children('.name_product_row').html();
            
                units_row=parseInt($(this).children('.units_product_row').html());
                
                tax_name=$(this).children('.tax_name').val();
                
                tax_percent=parseInt($(this).children('.tax_percent').val());
                
                if(!total_tax_name_dict.hasOwnProperty(tax_name))
                {
                
                    total_tax_name.push(tax_name+' '+tax_percent+'%');
                    total_tax_name_dict[tax_name]=1;
                    
                }
                
                
                real_price=parseInt($(this).children('.total_price_real').val());
                
                real_price_units=real_price*units_row;
                
                neto_price=Math.floor(real_price/((tax_percent/100)+1));
                
                percent_tax=real_price_units-neto_price;
                
                discount_percent=parseInt($(this).children('.discount_percent').val());
                
                total_tax_price=percent_tax;               
                
                total_price_discount=(neto_price)/100;
                
                discount_price=0;
                
                data['products'].push({name: name_product, product_id: product_id, units: units_row, price: (neto_price/100), total_price: (real_price/100), tax_name: tax_name, tax_percent: tax_percent, discount_percent: discount_percent, discount_price: discount_price, total_price_discount: total_price_discount, table_id: table_id});
                
                z=data['products'].length;
                
                percent_discount=0;
                
                if(discount_percent>0)
                {
                    
                    percent_discount=(neto_price/100)*discount_percent;
                    neto_price-=percent_discount;
                    
                    percent_tax=(neto_price/100)*tax_percent;
                    
                    final_price_discount+=percent_discount;
                    
                    real_price_units=neto_price+percent_tax;
                    
                    total_tax_price=percent_tax;
                    
                    discount_price=total_tax_price;
                
                    data['products'][z-1]['total_price_discount']=(neto_price)/100;
                       
                }
                
                data['products'][z-1]['tax_price']=total_tax_price/100;
                
                data['products'][z-1]['discount_price']=(percent_discount)/100;
                
                data['products'][z-1]['total_price_with_all']=(real_price_units)/100;
                
                final_tax_price+=total_tax_price;
                
                final_neto_price+=neto_price;
                
            });
            
            data['tax_name']=total_tax_name.join();
            
            data['tax_price']=final_tax_price/100;
            
            data['neto_price']=final_neto_price/100;

            data['discount_price']=final_price_discount/100;
            
            return data;
            
        }
        
        function set_save_products(payment_id, yes_payment, data)
        {
            data['bill_id']=payment_id;
            
            $.ajax({
                type: "POST",
                url: "<?php echo Routes::get_url('tpv/index/save_payment'); ?>",
                data: data,
                success: function (data) {
                    
                    //Add csrf token
                    
                    $('#csrf_token_field_form').attr('value', data.csrf_token);
                    
                    if(data.error==0)
                    {
                        
                        $('#option_bill_'+data.bill_id).remove();
                        
                        pending_orders();
                    
                        $('#payment_id').val(data.bill_id);
                    
                        if(yes_payment)
                        {
                        
                            choose_payment();
                            
                        }
                        else
                        {
                            
                            clean_box();
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        alert(data.txt_error);
                        
                    }
                    
                },
                error: function (data) {
                    
                    $('#csrf_token_field_form').attr('value', data.csrf_token);
                    
                    alert(JSON.stringify(data));
              
                },
                dataType: 'json'
            });
            
        }
        
        function choose_payment()
        {
            
            vex.dialog.open({
                message: 'Tipo de pago',
                input: [
                    'Efectivo: <input type="radio" name="payment_type" value="0" /><br />',
                    'Tarjeta: <input type="radio" name="payment_type" value="1" />'
                ].join(''),
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Submit' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
                ],
                callback: function (data) {
                    if (data) {
                        
                        if(data.payment_type==0)
                        {
                            
                            dialog_payment();
                            
                        }
                        else
                        {
                            
                            //Salvar como pagado
                            
                            make_payment(0);
                            
                            print_box();
                            
                        }
                        
                    }
                }
            });
            
        }
        
        function dialog_payment()
        {
            
            //Need a bill created.
            
            vex.dialog.open({
                message: 'Caja',
                input: [
                    'Dinero entregado: &euro; <input type="text" name="payment" value="0" required/>'
                ].join(''),
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Submit' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'Back' })
                ],
                callback: function (data) {
                    if (data) {
                        
                        if(data.payment>0)
                        {
                            
                            //Other dialog 
                            
                            change=data.payment-parseInt($('#total_price_bill_raw').val())/100;
                            
                            if(change>0)
                            {
                            
                                formatted_change=change.toFixed(2).replace('.', ',')
                            
                                show_change(data.payment, formatted_change);
                                
                            }
                            else
                            {
                                
                                dialog_payment();
                                
                            }
                            
                        }
                        
                    }
                }
            });
            
        }
        
        function show_change(total, change)
        {
            
            vex.dialog.open({
                message: 'Caja',
                input: [
                    '<input type="hidden" name="money_paid" value="'+total+'"/>Dinero a devolver: &euro; '+change
                ].join(''),
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Efectuar pago' })
                ],
                callback: function (data) {
                        
                    //Salvar como pagado.
                    
                    make_payment(data.money_paid);
                    
                    print_box();
                }
            });
            
        }
        
        function clean_box()
        {
            
            $('.product_row_added').each(function( index ) {
               
               $(this).remove();
                
            });
            
            $('#total_price_no_taxes_bill').html('0,00');
            
            $('#total_taxes_price').html('0,00');
            
            $('#total_price_bill').html('0,00');
            
            $('#total_price_bill_raw').val('0,00');
            
            $('#total_discounts').html('0,00');
            
            $('#payment_id').val('0');
            
            $('#added_new_value').val('0');
            
            $.ajax({
                url: "<?php echo Routes::get_url('tpv/index/get_total_billed'); ?>",
                data: {},
                success: function (data) {
                    
                    total_billed=((data.total_billed_raw)/100).toFixed(2).replace('.', ',');
                    
                    $('#total_money_billed_showed').html(total_billed+' €');
                    
                },
                dataType: 'json'
            });
            
        }
        
        function print_box()
        {
            //if enabled print ticket
            
            payment_id=parseInt($('#payment_id').val());
            
            if(payment_id>0)
            {
            
                window.open("<?php echo Routes::get_url('tpv/index/ticket_print'); ?>get/bill_id/"+payment_id, 'print_ticket', 'width=450,height=450');
                
            }
            
            clean_box();
            
            /*
            vex.dialog.open({
                message: '¿Imprimir ticket?',
                input: '',
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: 'Sí' }),
                    $.extend({}, vex.dialog.buttons.NO, { text: 'No' })
                ],
                callback: function (data) {

                    if(data)
                    {
                        
                        
                        
                    }

                    clean_box();
                }
            });*/
            
            
        }
        
        function make_payment(money_paid)
        {
            
            payment_id=parseInt($('#payment_id').val());
            
            if(payment_id>0)
            {
                
                data={payment_id: payment_id, money_paid: money_paid};
                
                $.ajax({
                    url: "<?php echo Routes::get_url('tpv/index/make_payment'); ?>",
                    data: data,
                    success: function (data) {
                        
                        
                    },
                    error: function (data) {
                        
                        alert(JSON.stringify(data));
                        
                    },
                    dataType: 'json'
                });
            }
        }
        
        function pending_orders()
        {

            data={};
            
            $.ajax({
                url: "<?php echo Routes::get_url('tpv/index/get_pending_orders'); ?>",
                data: data,
                success: function (data) {
                    
                    $('#pending_orders').html(data.bills.length);
                    
                    if(data.error==0)
                    {
                        
                        arr_product_exists={};
                        
                        for(k in data.bills)
                        {
                        
                            arr_product_exists[data.bills[k].id]=1;
                        
                            if($('#option_bill_'+data.bills[k].id).length == 0) {
                                
                                formatted_price=((data.bills[k].total_price)/100).toFixed(2).replace('.', ',');
                                
                                //((final_price)/100).toFixed(2).replace('.', ',')
                        
                                $('#select_bill').append('<a href="#" class="get_bill" id="option_bill_'+data.bills[k].id+'">'+data.bills[k].table_id_name+' -  € '+formatted_price+'</a>');
                                
                                $('#option_bill_'+data.bills[k].id).click( function () {
                                    
                                    id=$(this).attr('id').replace('option_bill_', '');

                                    /*
                                    data={};

                                    $.ajax({
                                        url: "<?php echo Routes::get_url('tpv/index/get_order'); ?>get/bill_id/"+id,
                                        data: data,
                                        success: function (data) {
                                            
                                            clean_box();
                                            
                                            $('#payment_id').val(data.bill_id);
                                            $('#table_id_field_form').val(data.table_id);
                                            
                                            for(k in data.products)
                                            {
                                                
                                                product_id=data.products[k].product_id;
                                                name=data.products[k].name;
                                                tax_percent=data.products[k].tax_percent;
                                                tax_name=data.products[k].tax_name;
                                                units=data.products[k].units;
                                                base_price_id=data.products[k].total_price;
                                                real_price=base_price_id*units;

                                                if(name.length>20)
                                                {
                                                    
                                                     name = jQuery.trim(name).substring(0, 20).split(" ").slice(0, -1).join(" ") + " [...]";
                                                    
                                                }

                                                new_row=$('#product_row').clone().appendTo('#products_list');
                                                new_row.children('.name_product_row').html(name);
                                                new_row.children('.units_product_row').html(1);
                                                new_row.children('.units_product_row').html(units);
                                                
                                                if(tax_percent>0)
                                                {
                                                    
                                                    //percent_tax=(base_price_id/100)*tax_percent;
                                                    neto_price=real_price/((tax_percent/100)+1);
                                                    
                                                    percent_tax=real_price-neto_price;
                                                    
                                                    formatted_price=(neto_price/100).toFixed(2).replace('.', ',');
                                                    
                                                    formatted_tax=(percent_tax/100).toFixed(2).replace('.', ',');
                                                    
                                                    new_row.children('.total_price_row').html('€ '+formatted_price+' '+tax_name+' '+tax_percent+'% € '+formatted_tax);
                                                    
                                                }
                                                else
                                                {
                                                    
                                                    formatted_price=(real_price/100).toFixed(2).replace('.', ',');
                                                    
                                                    new_row.children('.total_price_row').html('€ '+formatted_price+' ');
                                                }

                                                new_row.children('.total_price_real').val(base_price_id);
                                                new_row.children('.tax_name').val(tax_name);
                                                new_row.children('.tax_percent').val(tax_percent);
                                                new_row.addClass('product_row_added');
                                                
                                                new_row.css('display', 'block');
                                                new_row.attr('id', 'product_row_'+product_id);
                                                
                                                //$(new_row).click(function () {                       
                                                $(document).on( 'click', '#product_row_'+product_id, function(){

                                                    $(this).toggleClass('active_row');
                                                
                                                });
                                                
                                            }
                                            
                                            recalculate_prices();
                                            
                                            $('#added_new_value').val('0');

                                        },
                                        error: function (data) {
                                            
                                            alert(JSON.stringify(data));
                                            
                                        },
                                        dataType: 'json'
                                    });
                                    */
                                    
                                    edit_order(id);
                                    
                                    $('#show_pending').click();
                                    
                                    return false;
                                
                                });
                            }
                        }
                        
                        $('.get_bill').each( function () {
                        
                            id=$(this).attr('id').replace('option_bill_', '');
                        
                            if(!arr_product_exists.hasOwnProperty(id))
                            {

                                $(this).remove();

                            }

                        
                        });
                        
                    }
                    
                },
                error: function (data) {
                    
                    alert(JSON.stringify(data));
                    
                },
                dataType: 'json'
            });
            
        }
        
        function edit_order(id)
        {
            
            data={};

            $.ajax({
                url: "<?php echo Routes::get_url('tpv/index/get_order'); ?>get/bill_id/"+id,
                data: data,
                success: function (data) {
                    
                    clean_box();
                    
                    $('#payment_id').val(data.bill_id);
                    $('#table_id_field_form').val(data.table_id);
                    
                    for(k in data.products)
                    {
                        
                        product_id=data.products[k].product_id;
                        name=data.products[k].name;
                        tax_percent=data.products[k].tax_percent;
                        tax_name=data.products[k].tax_name;
                        units=data.products[k].units;
                        base_price_id=data.products[k].total_price;
                        real_price=base_price_id*units;
                        discount_percent=data.products[k].discount_percent;

                        if(name.length>20)
                        {
                            
                             name = jQuery.trim(name).substring(0, 20).split(" ").slice(0, -1).join(" ") + " [...]";
                            
                        }

                        new_row=$('#product_row').clone().appendTo('#products_list');
                        new_row.children('.name_product_row').html(name);
                        new_row.children('.units_product_row').html(1);
                        new_row.children('.units_product_row').html(units);
                        new_row.children('.discount_percent').val(discount_percent);
                        
                        if(tax_percent>0)
                        {
                            
                            //percent_tax=(base_price_id/100)*tax_percent;

                            neto_price=Math.floor(real_price/((tax_percent/100)+1));

                            percent_tax=real_price-neto_price;
                            
                            formatted_price=(neto_price/100).toFixed(2).replace('.', ',');
                            
                            if(discount_percent>0)
                            {
                                
                                //Add percent
                                
                                percent_discount=(neto_price/100)*discount_percent;
                                
                                neto_price=neto_price-percent_discount;

                                final_discount=(percent_discount/100).toFixed(2).replace('.', ',');
                                
                                formatted_price=(neto_price/100).toFixed(2).replace('.', ',');
                                
                                formatted_price=formatted_price+' <?php echo I18n::lang('phangoapp/tpv', 'discount', 'Discount'); ?> '+discount_percent+'% '+' € '+(percent_discount/100).toFixed(2).replace('.', ',');
                                
                                //Recalculate tax
                                
                                percent_tax=(neto_price/100)*tax_percent;
                            
                                //neto_price=neto_price-percent_tax;
                                
                            }
                            
                            formatted_tax=(percent_tax/100).toFixed(2).replace('.', ',');
                            
                            new_row.children('.total_price_row').html('€ '+formatted_price+' '+tax_name+' '+tax_percent+'% € '+formatted_tax);
                            
                        }
                        else
                        {
                            
                            formatted_price=(real_price/100).toFixed(2).replace('.', ',');
                            
                            if(discount_percent>0)
                            {
                                
                                //Add percent
                                
                                percent_discount=(neto_price/100)*discount_percent;
                                
                                neto_price=neto_price-percent_discount;
                                
                                final_discount=(percent_discount/100).toFixed(2).replace('.', ',');
                                
                                formatted_price=formatted_price+' <?php echo I18n::lang('phangoapp/tpv', 'discount', 'Discount'); ?> '+discount_percent+'% '+' € '+(percent_discount/100).toFixed(2).replace('.', ',');
                                
                            }
                            
                            new_row.children('.total_price_row').html('€ '+formatted_price+' ');
                        }

                        new_row.children('.total_price_real').val(base_price_id);
                        new_row.children('.tax_name').val(tax_name);
                        new_row.children('.tax_percent').val(tax_percent);
                        new_row.addClass('product_row_added');
                        
                        new_row.css('display', 'block');
                        new_row.attr('id', 'product_row_'+product_id);
                        
                        //$(new_row).click(function () {                       
                        $(document).on( 'click', '#product_row_'+product_id, function(){

                            $(this).toggleClass('active_row');
                        
                        });
                        
                    }
                    
                    recalculate_prices();
                    
                    $('#added_new_value').val('0');

                },
                error: function (data) {
                    
                    alert(JSON.stringify(data));
                    
                },
                dataType: 'json'
            });
            
        }
        
        pending_orders();
        
        setInterval(pending_orders, 5000);
        
        get_products(0);
        
        <?php
        
        if($_GET['bill_id']>0)
        {
            
        ?>
        edit_order(<?php echo $_GET['bill_id']; ?>);
        <?php
            
        }
        
        ?>
        
    </script>
</html>
<?php


}

?>
