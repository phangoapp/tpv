<?php

use PhangoApp\PhaModels\Forms;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaModels\Forms\MoneyForm;

function DiscountsProductsView($arr_product)
{
    
    ?>
    <p><a href="<?php echo AdminUtils::set_admin_link('tpv/products'); ?>"><?php echo I18n::lang('phangoapp/tpv', 'products', 'Products'); ?></a> &gt;&gt; <?php echo $arr_product['name']; ?></p>
    <h2><?php echo I18n::lang('phangoapp/tpv', 'discounts_of_product', 'Discounts of product'); ?></h2>
    <p><?php echo I18n::lang('phangoapp/tpv', 'adjust_discount_percent', 'If you need ajust the round of discount, use this formulary'); ?></p>
    <form method="post" class="form" action="<?php echo AdminUtils::set_admin_link('tpv/products', ['op' => 4, 'product_id' => $arr_product['id']]); ?>">
        <?php
        
        $arr_discount=json_decode($arr_product['discounts_applied'], true);

        foreach($arr_discount as $percent => $discount)
        {
            
            // [discount] => 0 [neto_price] => 2.89 [tax_percent] => 
            
            ?>
            <p><?php echo I18n::lang('phangoapp/tpv', 'discount', 'Discount'); ?> <?php echo $percent; ?>%: <input type="text" name="discount[<?php echo $percent; ?>]" value="<?php echo $discount['discount']; ?>" size="4" /> <?php echo I18n::lang('phangoapp/tpv', 'neto_price_discount', 'Neto price discount'); ?> : <input type="text" name="neto_price_discount[<?php echo $percent; ?>]" value="<?php echo $discount['neto_price']; ?>" size="4" /> <?php echo I18n::lang('phangoapp/tpv', 'tax_percent_discount', 'Tax percent discount'); ?> : <input type="text" name="tax_percent_discount[<?php echo $percent; ?>]" value="<?php echo $discount['tax_percent']; ?>" size="4" /> </p>
            <?php
            
        }

        ?>
        <?php echo PhangoApp\PhaUtils\Utils::set_csrf_key($name_token='csrf_token', $length_token=80); ?>
        </p>
        <p><input type="submit" value="<?php echo I18n::lang('phangoapp/tpv', 'round_price', 'Round price'); ?>" /></p>
    </form>
    <?php

}

?>
