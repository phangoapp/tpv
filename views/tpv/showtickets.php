<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaTime\DateTime;
use PhangoApp\PhaModels\CoreFields\MoneyField;

function ShowTicketsView($user, $list)
{

View::$js[]='jquery.min.js';
View::$css_module['tpv'][]='style_tpv.css';
View::$css_module['tpv'][]='vex.css';
View::$css_module['tpv'][]='vex-theme-os.css';
View::$js_module['tpv'][]='vex.combined.min.js';
View::$css[]='font-awesome.min.css';
    
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo I18n::lang('phangoapp/tpv', 'tpv', 'TPV'); ?> - <?php echo $user['enterprise_id_name']; ?></title>
    <?php echo View::load_css(); ?>
    <?php echo View::load_header(); ?> 
  </head>
  <body>
<nav class="navbar">
  <a class="logo_nav" href="<?php echo Routes::get_url('tpv/showtickets'); ?>">
        <?php echo I18n::lang('phangoapp/tpv', 'tpv', 'TPV'); ?> - <?php echo $user['enterprise_id_name']; ?> - <?php echo I18n::lang('phangoapp/tpv', 'last_orders', 'Last orders'); ?>
  </a>
<div id="header_options">
    <a id="close_box_button" href="<?php echo Routes::get_url('tpv'); ?>">
      <i class="fa fa-chevron-circle-left" aria-hidden="true"></i> <?php echo I18n::lang('phangoapp/tpv', 'go_back_box_button', 'Go back to box'); ?>
  </a>
  </div>
</nav>
    <div class="content" style="padding:5px";>
        <?php echo $list->show(); ?>
    </div>
    <?php echo View::load_js(); ?>
    <script>vex.defaultOptions.className = 'vex-theme-os'</script>
    <script>
        $('.cancel_bill').click( function () {
        
            id=$(this).attr('id').replace('cancel_bill_', '');
        
            vex.dialog.open({
                message: "<?php echo I18n::lang('phangoapp/tpv', 'cancel_payment', '¿Quiere cancelar este cobro?'); ?>",
                input: '<input type="hidden" name="bill_id" value="'+id+'" />',
                buttons: [
                    $.extend({}, vex.dialog.buttons.YES, { text: "<?php echo I18n::lang('common', 'yes', 'Yes'); ?>" }),
                    $.extend({}, vex.dialog.buttons.NO, { text: "<?php echo I18n::lang('common', 'no', 'No'); ?>" })
                ],
                callback: function (data) {
                    
                    if (data) {

                        location.href="<?php echo Routes::get_url('tpv/showtickets', [], ['op' => 2]); ?>/bill_id/"+id;
                        
                        return true;
                        
                    }
                    else
                    {
                        
                        return false;
                        
                    }
                }
            });
            
            return false;
        
        });
    </script>
</body>
</html>
<?php
}

?>
