<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaTime\DateTime;
use PhangoApp\PhaModels\CoreFields\MoneyField;
use PhangoApp\PhaUtils\SimpleTable;
use PhangoApp\PhaUtils\Pages;

function BillingDayView($admin, $month, $day, $real_day)
{

    $month_date=substr($real_day, 0, 6);

    ?>
    <h2><?php echo I18n::lang('phangoapp/tpv', 'bills_of', 'Bills of '); ?><?php echo $day; ?></h2>
    <p><a href="<?php echo AdminUtils::set_admin_link('billing/bills', ['enterprise_id' => $_GET['enterprise_id']]); ?>"><?php echo I18n::lang('phangoapp/tpv', 'bills', 'Bills'); ?></a> &gt;&gt; <a href="<?php echo AdminUtils::set_admin_link('billing/bills', ['op' => 1, 'month' => $month_date, 'enterprise_id' => $_GET['enterprise_id']]); ?>"><?php echo I18n::lang('phangoapp/tpv', 'bills_of', 'Bills of '); ?><?php echo $month; ?></a> &gt;&gt; <?php echo I18n::lang('phangoapp/tpv', 'bills_of', 'Bills of '); ?><?php echo $day; ?></p>
    <?php

    $admin->show();

}

?>
