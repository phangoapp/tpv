<?php

use PhangoApp\PhaModels\Forms;
use PhangoApp\PhaView\View;
use PhangoApp\PhaModels\ModelForm;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaLibs\AdminUtils;

function StockView($arr_product, $stock, $enterprise, $enterprise_id)
{
    
    View::$js[]='jquery.min.js';
    View::$js[]='posting.js';
    
    ob_start();
    ?>
    <script>
        
        $(document).ready( function () {
            //(url, update_id, update_waiting_id, error_text_id, pre_callback, success_callback) 
            $('#form_stock').sendPost("<?php echo PhangoApp\PhaLibs\AdminUtils::set_admin_link('tpv/stock', ['op' => 2, 'product_id' => $arr_product['id']]);?>",'#update_successfully', '.spinner', '#error_updating', false, false);
           
           $('#enterprise_id_selected_field_form').change( function () {
              
              enterprise_id=$(this).val();
              
                if(enterprise_id>0)
                {
              
                    $.ajax({
                      url: "<?php echo PhangoApp\PhaLibs\AdminUtils::set_admin_link('tpv/stock', ['op' => 1, 'product_id' => $arr_product['id']]);?>",
                      data: {enterprise_id: enterprise_id},
                      success: function (data) {
                          
                            $('#avaliable_field_form').val(data.avaliable);
                          
                            $('#enterprise_id_field_form').val(enterprise_id);
                      
                            //$('#form_stock').show();
                          
                        },
                        error: function (data) {
                            
                            alert(JSON.stringify(data));
                            
                        },
                        fail: function (data) {
                            
                            alert(JSON.stringify(data));
                            
                        },
                      dataType: 'json'
                    });
               
                }
                else
                {
                
                    //$('#form_stock').hide();
                    $('#avaliable_field_form').val('');
                    $('#enterprise_id_field_form').val('');
                    
                }
               
           });
            
        });
        
    </script>
    <?php
    View::$header[]=ob_get_contents();
    
    ob_end_clean();
    
    $form_enterprise=new Forms\SelectModelForm('enterprise_id_selected', $enterprise_id, new Enterprise(), 'name', 'id', ['WHERE 1=1 order by name ASC', []], 1);
    
    $form_enterprise->default_value=$enterprise_id;
    
    ?>
    <p><a href="<?php echo AdminUtils::set_admin_link('tpv/products'); ?>"><?php echo I18n::lang('phangoapp/tpv', 'products', 'Products'); ?></a> &gt;&gt; <?php echo I18n::lang('phangoapp/tpv', 'stock_of', 'Stock of'); ?> <?php echo $arr_product['name']; ?></p>
    <p>Elija empresa: <?php echo $form_enterprise->form(); ?></p>
    <form id="form_stock" name="form_stock" method="post" autocomplete="off" action="#">
        <?php
        
        $stock->forms['product_id']->default_value=$arr_product['id'];
        
        echo ModelForm::show_form($stock->forms, [], $pass_values=false, $check_values=false, $keys_form=[]);
        ?>
        <p><input type="submit" value="<?php echo I18n::lang('phangoapp/tpv', 'add_stock', 'Add stock'); ?>" /></p>
    </form>
    <div class="spinner" id="spinner" style="display:none;">
      <div class="rect1"></div>
      <div class="rect2"></div>
      <div class="rect3"></div>
      <div class="rect4"></div>
      <div class="rect5"></div>
    </div>
    <div id="update_successfully" class="flash" style="display:none;">
        <?php echo I18n::lang('phangoapp/tpv', 'done_stock', 'The stock was updated successfully'); ?>
    </div>
    <div id="error_updating" class="flash" style="display:none;">
        <?php echo I18n::lang('phangoapp/tpv', 'error_stock', 'Error: the stock cannot be updated. Do you chose a enterprise?'); ?>
    </div>
    <p></p>
    <?php
}

?>
