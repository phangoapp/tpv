<?php

use PhangoApp\PhaView\View;
use PhangoApp\PhaI18n\I18n;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaUtils\Utils;
use PhangoApp\PhaLibs\AdminUtils;
use PhangoApp\PhaTime\DateTime;
use PhangoApp\PhaModels\CoreFields\MoneyField;
use PhangoApp\PhaUtils\SimpleTable;
use PhangoApp\PhaUtils\Pages;

function BillingView($total_price, $total_price_neto, $total_price_tax, $total_price_month, $total_price_day, $arr_month, $total_months)
{

?>
<h2 style="border: solid #cbcbcb 1px;padding:6px;color:#fbfbfb;background: #000;"><?php echo I18n::lang('phangoapp/tpv', 'total_billed_since_begin', 'Total billed'); ?>: <?php echo $total_price; ?> <?php echo I18n::lang('phangoapp/tpv', 'neto_billed', 'Neto billed'); ?>: <?php echo $total_price_neto; ?> <?php echo I18n::lang('phangoapp/tpv', 'taxes_billed', 'Taxes billed'); ?>: <?php echo $total_price_tax; ?> </h2>
<h2 style="border: solid #cbcbcb 1px;padding:6px;color:#fbfbfb;background: #000;"><?php echo I18n::lang('phangoapp/tpv', 'total_billed_this_month', 'Total billed in actual month'); ?>: <?php echo $total_price_month; ?></h2>
<h2 style="border: solid #cbcbcb 1px;padding:6px;color:#fbfbfb;background: #000;"><?php echo I18n::lang('phangoapp/tpv', 'total_billed_this_day', 'Total billed in actual day'); ?>: <?php echo $total_price_day; ?></h2>
<h2><?php echo I18n::lang('phangoapp/tpv', 'billing_by_month', 'Billing by months'); ?></h2>
<?php

SimpleTable::top_table_config([I18n::lang('phangoapp/tpv', 'billed', 'Billed'), I18n::lang('phangoapp/tpv', 'month', 'Month'), I18n::lang('common', 'options', 'Options')]);

foreach($arr_month as $month)
{
    
$m=substr(date($month[0]), 4, 2)-1;

$months=['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

$options='<a href="'.AdminUtils::set_admin_link('billing/bills', ['month' => $month[0], 'op' => 1, 'enterprise_id' => $_GET['enterprise_id']]).'">'.I18n::lang('phangoapp/tpv', 'view_bills_of_months', 'View bills of month').'</a>';

SimpleTable::middle_table_config([$month[1], $months[$m], $options]);
    
}

SimpleTable::bottom_table_config();

?>
<?php echo I18n::lang('common', 'pages', 'Pages'); ?>: <?php echo Pages::show( $_GET['begin_page'], $total_months, 12,  AdminUtils::set_admin_link('billing/bills', []), $initial_num_pages=20, $variable='begin_page', $label='', $func_jscript=''); ?>
<?php

}

?>
