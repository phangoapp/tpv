<?php

use PhangoApp\PhaModels\Forms;

function ProductsView($admin, $category_id)
{
    
    $form_category=new Forms\SelectModelFormByOrder('parent', '', new CategoryProduct(), 'name', 'parent', $where=['WHERE 1=1', []], $null_yes=1);
    
    $form_category->default_value=$category_id;
    
    ?>
    <form method="get" action="<?php echo PhangoApp\PhaLibs\AdminUtils::set_admin_link('tpv/products'); ?>">
        <p>Elegir categoría: <?php echo $form_category->form(); ?> <input type="submit" value="<?php echo PhangoApp\PhaI18n\I18n::lang('phangoapp\tpv', 'change_category', 'Cambiar categoría'); ?>" /></p>
    </form>
    <?php

    $admin->show();

}

?>
