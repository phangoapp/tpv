<?php

use PhangoApp\PhaModels\Webmodel;
use PhangoApp\PhaModels\CoreFields;
use PhangoApp\PhaRouter\Routes;
use PhangoApp\PhaModels\ExtraModels\UserPhangoModel;

class CategoryProduct extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('parent', new CoreFields\ParentField($size=11, $name_field='parent', $name_value='name'));
        $this->register('tax_id', new CoreFields\ForeignKeyField(new Taxes(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        
    }
    
}

//Products.

class Products extends Webmodel {

    public function insert($post, $safe_query=0, $cache_name='')
	{
        
        $yes_insert=parent::insert($post, $safe_query, $cache_name);
        
        if($yes_insert)
        {
            
            $insert_id=$this->insert_id();
            
            $tax=new Taxes();
            
            $arr_tax=$tax->select_a_row($post['tax_id']);
            
            $neto_price=$post['base_price'];
            
            $tax_percent=0;
            
            $product=$this;
            
            if($arr_tax)
            {
                
                //Calculate price without tax
                
                $neto_price=round($post['base_price']/(($arr_tax['percent']/100)+1), 2);
                
                //Calculate tax percent
                
                $tax_percent=$post['base_price']-$neto_price;
                                
            }
            
            $arr_discount_percent[0]['discount']=0;
            $arr_discount_percent[0]['neto_price']=$neto_price;
            $arr_discount_percent[0]['tax_percent']=0;
            
            for($x=1;$x<=100;$x++)
            {
                
                $arr_discount_percent[$x]['discount']=round($neto_price*($x/100), 2);
                $arr_discount_percent[$x]['neto_price']=$neto_price-$arr_discount_percent[$x]['discount'];
                $arr_discount_percent[$x]['tax_percent']=round($arr_discount_percent[$x]['neto_price']*($arr_tax['percent']/100), 2);
                
            }
            
            $product->reset_require();
                
            $product->where(['WHERE id=?', [$insert_id]])->update(['tax_applied' => ['neto_price' => $neto_price, 'tax_percent' => $tax_percent], 'discounts_applied' => $arr_discount_percent]);
                
            $product->reload_require();
            
        }
        
        return $yes_insert;
        
    }
    
    public function update($post, $safe_query=0, $cache_name='')
	{
        
        $old_conditions=$this->conditions;
        
        $arr_row=$this->select_a_row($_GET['id'], ['base_price']);
        
        $base_price=$this->components['base_price']->check($post['base_price']);
        
        $arr_row['base_price']=$this->components['base_price']->check($arr_row['base_price']);
        
        $update_taxes=false;

        if($arr_row['base_price']!=$base_price)
        {
            
            $update_taxes=true;
            
        }

        $this->conditions=$old_conditions;
        
        $yes_update=parent::update($post, $safe_query, $cache_name);
        
        if($yes_update && $update_taxes)
        {
            
            
            $tax=new Taxes();
            
            $arr_tax=$tax->select_a_row($post['tax_id']);
            
            $neto_price=$base_price;
            
            $tax_percent=0;
            
            if($arr_tax)
            {
                
                //Calculate price without tax
                
                $neto_price=round($base_price/(($arr_tax['percent']/100)+1), 2);
                
                //Calculate tax percent
                
                $tax_percent=$base_price-$neto_price;
                
            }
            
            $arr_discount_percent[0]['discount']=0;
            $arr_discount_percent[0]['neto_price']=$neto_price;
            $arr_discount_percent[0]['tax_percent']=0;
            
            for($x=1;$x<=100;$x++)
            {
                
                $arr_discount_percent[$x]['discount']=round($neto_price*($x/100), 2);
                $arr_discount_percent[$x]['neto_price']=$neto_price-$arr_discount_percent[$x]['discount'];
                $arr_discount_percent[$x]['tax_percent']=round($arr_discount_percent[$x]['neto_price']*($arr_tax['percent']/100), 2);
                
            }
            
            $this->reset_require();
            
            $this->set_conditions(['WHERE id=?', [$_GET['id']]]);
            
            parent::update(['tax_applied' => ['neto_price' => $neto_price, 'tax_percent' => $tax_percent], 'discounts_applied' => $arr_discount_percent ] );
                
            $this->reload_require();
            
        }
        
        return $yes_update;
        
    }

    //Name, base price, image. 
    public function load_components()
    {
        
        $this->change_id_default('id');

        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('base_price', new CoreFields\MoneyField() , true);
        $this->register('image', new CoreFields\ImageField('./files/images/products', Routes::$root_url.'files/images/products', $thumb=1, $img_width=array('mini' => 150), $quality_jpeg=85) , true);
        $this->register('category_id', new CoreFields\ForeignKeyField(new CategoryProduct(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        $this->register('units_solded', new CoreFields\IntegerField());
        $this->register('tax_id', new CoreFields\ForeignKeyField(new Taxes(), $size=11, $default_id=0, $name_field='id', $select_fields=['name', 'percent']), true);
        $this->register('tax_applied', new CoreFields\ArrayField(new CoreFields\CharField()));
        $this->register('discounts_applied', new CoreFields\ArrayField(new CoreFields\CharField()));
        
    }

}

//Stock

class Stock extends Webmodel {

    
    public function load_components()
    {
        
        $this->change_id_default('id');
        //$this->register('number', new CoreFields\IntegerField(11));
        $this->register('avaliable', new CoreFields\BooleanField());
        
        $this->register('product_id', new CoreFields\ForeignKeyField(new Products(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        
    }
    
}

class Enterprise extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('bussiness_name', new CoreFields\CharField(255) , true);
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('nif', new CoreFields\CharField(25) , true);
        $this->register('address', new CoreFields\CharField(1000) , true);
        $this->register('country', new CoreFields\CharField(255) , true);
        $this->register('phone', new CoreFields\PhoneField(255));
        $this->register('footer_ticket', new CoreFields\CharField(1024));
        
        
    }
    
}

class ProductEnterprise extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('product_id', new CoreFields\ForeignKeyField(new Products(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        $this->register('stock', new CoreFields\IntegerField(11) , true);
        
    }
    
}

class Taxes extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('percent', new CoreFields\PercentField() , true);
        
    }
    
}

class UserEnterprise extends UserPhangoModel {
    
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('username', new CoreFields\CharField(255) , true);
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('email', new CoreFields\EmailField(255) , true);
        $this->register('password', new CoreFields\PasswordField() , true);
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='id', $select_fields=['name']), true);
        $this->register('login_token', new CoreFields\CharField(255));
    }
    
}

class Bill extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('money_paid', new CoreFields\MoneyField());
        $this->register('total_price', new CoreFields\MoneyField());
        $this->register('neto_price', new CoreFields\MoneyField());
        $this->register('tax_name', new CoreFields\CharField(1024));
        $this->register('tax_price', new CoreFields\MoneyField());
        $this->register('discount_price', new CoreFields\MoneyField());
        $this->register('payment', new CoreFields\BooleanField());
        $this->register('cancelled', new CoreFields\BooleanField());
        $this->register('date', new CoreFields\DateField());
        $this->register('table_id', new CoreFields\ForeignKeyField(new TableBar(), $size=11, $default_id=0, $name_field='name', $select_fields=['id', 'name']));
        $this->register('openbox_id', new CoreFields\ForeignKeyField(new OpenBox(), $size=11, $default_id=0, $name_field='date', $select_fields=['id', 'date']));
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='name', $select_fields=['id']));
        $this->register('month', new CoreFields\IntegerField());
        
    }
    
}

class ProductBill extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('product_id', new CoreFields\IntegerField() , true);
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('units', new CoreFields\IntegerField() , true);
        $this->register('price', new CoreFields\MoneyField() , true);
        $this->register('total_price', new CoreFields\MoneyField() , true);
        $this->register('total_price_units', new CoreFields\MoneyField() , true);
        //Equal to units
        $this->register('total_price_discount', new CoreFields\MoneyField());
        $this->register('discount_price', new CoreFields\MoneyField());
        $this->register('discount_percent', new CoreFields\PercentField());
        $this->register('tax_name', new CoreFields\CharField());
        $this->register('tax_percent', new CoreFields\PercentField());
        $this->register('tax_price', new CoreFields\MoneyField());
        $this->register('total_price_with_all', new CoreFields\MoneyField() , true);
        $this->register('bill_id', new CoreFields\ForeignKeyField(new Bill(), $size=11, $default_id=0, $name_field='id', $select_fields=['id', 'table_id']), true);
        
    }
    
}

class TableBar extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('name', new CoreFields\CharField(255) , true);
        $this->register('default', new CoreFields\BooleanField());
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='name', $select_fields=['id']), true);
        
    }
    
}

class OpenBox extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('date', new CoreFields\DateField());
        $this->register('closed', new CoreFields\BooleanField());
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='name', $select_fields=['id']));
        
    }
    
}

class LogTpv extends Webmodel {
    
    public function load_components()
    {
        
        $this->change_id_default('id');
        $this->register('message', new CoreFields\CharField(1024) , true);
        $this->register('date', new CoreFields\DateField());
        $this->register('enterprise_id', new CoreFields\ForeignKeyField(new Enterprise(), $size=11, $default_id=0, $name_field='name', $select_fields=['id']));
                
    }
    
    public function log($message)
    {
        
        $this->fields_to_update= ['message', 'date', 'enterprise_id'];
        
        $post['date']=0;
        $post['message']=$message;
        $post['enterprise_id']=$_SESSION['enterprise_id'];
        
        return $this->insert($post);
        
    }
    
}

//Taxes to apply in other table.

//Facturacion

//Descuentos, tipos de descuento.

//Activar tickets en un determinado terminal. Con ajax, el terminal que tenga permiso para imprimir se encarga de ello.

//Empresas, cada empresa tiene su tabla. Cada stock puede tener su empresa. Una sección de inventario.

//UserEnterprise: usuario para el tpv

?>
