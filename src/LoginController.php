<?php

namespace PhangoApp\Tpv;

use PhangoApp\PhaRouter\Controller;
use PhangoApp\PhaRouter\Routes;

class LoginController extends Controller {

    public function __construct(  $route,   $name,   $yes_view = 1) 
    {
        
        parent::__construct($route, $name, $yes_view);
        
        if(!isset($_SESSION['login_tpv']))
        {
            
            header('Location: '.Routes::get_url('tpv/login'));
            
            die;
            
        }
    
    }

}


?>
